init('FlairSelector', function(opts) {
    (window.FlairSelector = {
        state: {
            previous_selected_element: null,
            disabled: opts.disabled,
            documents: map(document.querySelectorAll('.flair-table .flair-option'), function(el) {
                return {
                    name: el.getAttribute('data-name'),
                    display_name: el.getAttribute('data-displayname'),
                    category: el.getAttribute('data-category'),
                }
            }),
            search_index: null,
        },
        select: function(element, flair_class) {
            if (element == this.state.previous_selected_element || this.state.disabled === true) {
                return;
            } else if (n.hasClass(element, 'flair-option-disabled')) {
                return;
            }

            if (this.state.previous_selected_element) {
                n.removeClass(this.state.previous_selected_element, 'selected');
            }

            n.value('[name=flair_class]', flair_class);
            n.html('.flair-display-cell', n.addClass(element.cloneNode(true), 'flair-option-disabled'));
            n.disabled('.flair-submit-button', false);
            n.addClass(element, 'selected');

            this.state.previous_selected_element = element;
        },
        clear_search: function() {
            this._listeners.searchbox.do_search('');
        },
        _listeners: {
            searchbox: {
                el: '#flair-search-text',
                ev: 'keyup',
                fn: function(event) {
                    if (this.timeout) {
                        clearTimeout(this.timeout);
                        this.timeout = null;
                    }

                    var search_text = this.value;

                    if (!FlairSelector.state.search_index) {
                        return;
                    }

                    this.timeout = setTimeout(function() {
                        FlairSelector._listeners.searchbox.do_search(search_text);
                    }, 200);
                },
                timeout: null,
                previous_result_elements: [],
                do_search: function(search_text) {
                    var table   = document.querySelector('.flair-table'),
                        noRes   = document.querySelector('.flair-searchbox__NoResults');

                    n.hide(noRes);
                    n.show(table);

                    n.removeAttribute(this.previous_result_elements, 'style');
                    this.previous_result_elements = [];

                    if (!search_text) {
                        n.removeClass(table, 'flair-search-active');
                        return;
                    }

                    search_text = rtrim(search_text, '*');

                    search_text = search_text + '^2 ' + rtrim(search_text, '*') + '*' + ' ' + ltrim(search_text, '*') + '~1';

                    var results;

                    try {
                        results = FlairSelector.state.search_index.search(search_text);
                    } catch(error) {
                        results = [];
                    }

                    if (!results.length) {
                        n.show(noRes);
                        n.hide(table);
                    } else {
                        n.addClass(table, 'flair-search-active')
                    }

                    var idx = 0;

                    this.previous_result_elements = map(results, function(item) {
                        var element = document.getElementById('FlairOption--' + item.ref);
                        element.setAttribute('style', 'display:inherit;order:'+(idx++));
                        return element;
                    });
                }
            },
            checkform: {
                el: '#FlairSelectorForm',
                ev: 'submit',
                fn: function(event) {
                    if (!document.getElementsByName('flair_class')[0].value.length) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                },
            },
            selectoption: {
                el: '.flair-option',
                ev: 'click',
                fn: function(event) {
                    FlairSelector.select(this, this.getAttribute('data-flairclass'));
                },
            },
        },
        init: function() {
            forEach(this._listeners, function(key, data) {
                n.on(data.el, data.ev, data.fn);
            });
            this.state.search_index = lunr(function () {
                var instance = this;

                this.ref('name');
                this.field('display_name');
                this.field('category');

                forEach(window.FlairSelector.state.documents, function(doc) {
                    instance.add(doc);
                }, this);
            });
        }
    }).init();
});
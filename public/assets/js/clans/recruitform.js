init('RecruitForm', function(opts) {
    if (opts.submitted || !opts.logged_in) {
        return;
    }

    (function() {
        var mapElement = document.getElementById('RegionSelector__map');
        var countryList = document.querySelectorAll('[data-continent]');
        var regionOptions = document.querySelectorAll('.RegionOption');
        var regionsEnabled = 0;

        function toggleRegionOption(regionOption, is_map_toggle, force_on, force_off) {
            var continent = regionOption.getAttribute('data-value');
            var continent_id = continent.replace(' ', '_');

            if (force_off || (n.hasClass(regionOption, 'selected') && !force_on)) {
                if (n.hasClass(regionOption, 'selected')) {
                    regionsEnabled--;
                }

                n.removeClass(regionOption, 'selected');
                n.removeClass(mapElement, 'selected--' + continent_id);
                if (is_map_toggle)
                    n.addClass(mapElement, 'hovered--' + continent_id);
                regionOption.querySelector('input').value = '';
            } else {
                if (!n.hasClass(regionOption, 'selected')) {
                    regionsEnabled++;
                }

                n.addClass(regionOption, 'selected');
                n.addClass(mapElement, 'selected--' + continent_id);
                regionOption.querySelector('input').value = continent;
            }

            if (regionsEnabled === 7) {
                document.querySelector('.RegionSelectAll span').innerHTML = 'Unselect All';
            } else {
                document.querySelector('.RegionSelectAll span').innerHTML = 'Select All';
            }
        }

        n.on('.RegionSelectAll', 'click', function() {
            if (regionsEnabled === 7) {
                for (var i = 0; i < regionOptions.length; i++) {
                    toggleRegionOption(regionOptions[i], false, false, true);
                }
            } else {
                for (var i = 0; i < regionOptions.length; i++) {
                    toggleRegionOption(regionOptions[i], false, true, false);
                }
            }
        });

        for (var i = 0; i < countryList.length; i++) {
            var country = countryList[i];

            country.setAttribute('data-continent-id',
                country.getAttribute('data-continent').replace(' ', '_'));

            n.on(country, 'mouseover', function(event) {
                var continent_id = this.getAttribute('data-continent-id');
                n.addClass(mapElement, 'hovered--' + continent_id);
            });
            n.on(country, 'mouseleave', function(event) {
                var continent_id = this.getAttribute('data-continent-id');
                n.removeClass(mapElement, 'hovered--' + continent_id);
            });
            n.on(country, 'click', function(event) {
                var continent = this.getAttribute('data-continent');
                var regionOption = document.querySelector('.RegionOption[data-value="'+continent+'"]');
                toggleRegionOption(regionOption, true);
            });
        }

        for (var i = 0; i < regionOptions.length; i++) {
            var regionOption = regionOptions[i];
            n.on(regionOption, 'click', function(event) {
                toggleRegionOption(this, false);
            });
        }
    })();

    (function() {
        var errors_enabled = 0;
        var is_submitting = false;

        function clear_errors() {
            var a = document.querySelectorAll('.field__errors');
            var b = document.querySelectorAll('.field_error');

            for (var i = 0; i < a.length; i++) {
                n.removeClass(a[i], 'enabled');
            }
            for (var i = 0; i < b.length; i++) {
                n.removeClass(b[i], 'enabled');
            }

            errors_enabled = 0;
        }

        function set_error_enabled(fieldname, errname, errstate) {
            errstate = is_undef(errstate) ? true : errstate; // default value -> true

            var errors = document.querySelector('.field[data-for="'+fieldname+'"] .field__errors');
            var error = errors.querySelector('[data-errname="'+errname+'"]');

            if (errstate) {
                if (n.hasClass(error, 'enabled')) {
                    return; // already enabled
                }
                n.addClass(error, 'enabled');
                n.addClass(errors, 'enabled');
                errors_enabled++;
            } else {
                if (!n.hasClass(error, 'enabled')) {
                    return; // already disabled
                }
                n.removeClass(error, 'enabled');
                if (!errors.querySelector('.enabled').length) {
                    n.removeClass(errors, 'enabled');
                }
                errors_enabled--;
            }
        }

        function is_bungie_url(url) {
            var domain = extract_domain(url).toLowerCase();
            if (domain == 'www.bungie.net' || domain == 'bungie.net') {
                return true;
            }
        }

        function form_validate(event) {
            if (is_submitting) {
                event.preventDefault();
                return;
            } else {
                is_submitting = true;
            }

            clear_errors();

            var field;

            // ---- BEGIN HELPER FUNCTIONS
            function field_input() {
                return document.querySelector('.field [name="'+field+'"]');
            }
            function field_required() {
                var has_value = true;
                if (field == 'platform_option') {
                    if (!document.querySelectorAll('.platform_option:checked').length) {
                        set_error_enabled(field, 'Required');
                        has_value = false;
                    }
                } else if (field == 'RegionOption[]') {
                    if (!document.querySelectorAll('.RegionOption.selected').length) {
                        set_error_enabled(field, 'Required');
                        has_value = false;
                    }
                } else if (!field_input().value.length) {
                    set_error_enabled(field, 'Required');
                    has_value = false;
                }
                return has_value;
            }
            // ----- END HELPER FUNCTIONS

            // ----- BEGIN VALIDATION
            {
                field ='bungie_profile'
                if (field_required()) {
                    if (!is_bungie_url(field_input().value)) {
                        set_error_enabled(field, 'BungieURL');
                    }
                }
            }
            {
                field = 'clan_name';
                if (field_required()) {
                    if (field_input().value.length > 50) {
                        set_error_enabled(field, 'MaxLengthExceeded');
                    }
                    if (field_input().value.length < 3) {
                        set_error_enabled(field, 'MinLengthNotMet');
                    }
                }
            }
            {
                field = 'platform_option';
                field_required();
            }
            {
                field = 'RegionOption[]';
                field_required();
            }
            {
                field = 'clan_desc';
                if (field_required()) {
                    if (field_input().value.length > 400) {
                        set_error_enabled(field, 'MaxLengthExceeded');
                    }
                    if (field_input().value.length < 25) {
                        set_error_enabled(field, 'MinLengthNotMet');
                    }
                }
            }
            {
                field = 'clan_link';
                if (field_required()) {
                    if (!is_bungie_url(field_input().value)) {
                        set_error_enabled(field, 'BungieURL');
                    }
                    var val = field_input().value.toLowerCase().split('bungie.net')[1];
                    if (!(!!val && (startsWith(val, '/en/clanv2?groupid=') || startsWith(val, '/en/clanv2/chat?groupid=')))) {
                        set_error_enabled(field, 'BungieURL');
                    }
                }
            }
            // ----- END VALIDATION

            if (errors_enabled && event)
                event.preventDefault();
            if (errors_enabled)
                is_submitting = false;
            return !errors_enabled;
        }

        n.on('#ClanRecruitmentForm', 'submit', function(event) {
            return form_validate(event);
        });
    })();
});
<div id="inlogin" class="wrapper halign">
    <div id="inlogin__Inner" class="wrapper halign">
        <div id="inlogin__Failed">
            <h2 id="inlogin__Title">Login Failed</h2>
            <div id="inlogin__Notice">
                <p>Failed to login: <?php echo x_stash('error') ?>.</p>
            </div>
            <a class="inlogin__Button" href="<?php echo SITE_URL ?><?php
                echo (is_stashed('relogin_link') ? x_stash('relogin_link') : 'login')
            ?>">Try again?</a>
        </div>
    </div>
</div>
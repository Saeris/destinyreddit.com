<div id="http-error" class="wrapper">
    <p id="tagline">403 Forbidden</p>
    <p>You are not allowed to access this page. If your session
    timed out, please <a href="<?php echo SITE_URL ?>login?cont=<?php echo current_url() ?>">login<a></p>
</div>

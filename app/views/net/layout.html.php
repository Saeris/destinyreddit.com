<div id="top"></div>
<noscript>
    <div class="error-notice">
        <p><strong>This website does not work without JavaScript.</strong></p>
    </div>
</noscript>
<header class="fsplit fsplit2">
    <div class="site-title">
        <h1>Destiny Reddit</h1>
    </div>
    <div class="user textAlignRight">
        <?php if (logged_in()): ?>
            <?php if (is_admin()): ?>
                <b class="admin-marker" style="margin-right:5px">(admin)</b>
            <?php endif; ?>
            <span>Welcome, /u/<?php echo get_username(); ?></span>
            <a href="<?php echo SITE_URL ?>logout?cont=<?php echo current_url() ?>">Logout</a>
        <?php else: ?>
            <a href="<?php echo SITE_URL ?>login?cont=<?php echo current_url() ?>">Login with Reddit</a>
        <?php endif; ?>
    </div>
</header>
<main><?php render_content_id(); ?></main>
<div><h1>Flair Selector</h1></div>
<style type="text/css"><?php echo u_stash('flair_css') ?></style>
<form id="FlairSelectorForm" class="flair-form" name="FlairSelect"
        method="POST" action="<?php echo SITE_URL ?>flair">
    <input type="hidden" name="flair_class" required />
    <div class="flair-display halign">
        <?php if (logged_in()): ?>
        <div class="flair-display-inner">
            <div class="flair-display-cell">
                <div class="flair-option flair-option-disabled">
                    <div class="flair flair-none"></div>
                    <div>None chosen</div>
                </div>
            </div>
            <div class="flair flair-submit-area halign">
                <?php if (is_admin()): ?>
                <div class="flair-submit-field">
                    <input type="text" name="flair_user" maxlength="20" placeholder="User (optional)"
                        value="<?php echo get_username() ?>" required />
                </div>
                <?php endif; ?>
                <div class="flair-submit-field">
                    <input type="text" name="flair_text" maxlength="64" placeholder="Flair hover text (optional)" />
                </div>
                <input class="flair-submit-button" type="submit" value="Submit" disabled />
            </div>
        </div>
        <?php else: ?>
        <p>Please log in to choose a flair</p>
        <?php endif; ?>
    </div>
    <div class="flair-searchbox">
        <input id="flair-search-text" type="text" placeholder="Search for a flair..." />
        <div class="flair-searchbox__NoResults" style="display:none">
            <p>No results</p>
        </div>
    </div>
    <div class="flair-table">
        <?php foreach (xa_stash('flair_data') as $name => $data): ?>
            <?php if (!$data['available'] && !is_admin()) continue; ?>
            <div class="flair-option" id="FlairOption--<?php echo $name; ?>"
                    data-name="<?php echo $name; ?>"
                    data-displayname="<?php echo $data['display_name'] ?>"
                    data-flairclass="<?php echo $data['flair_class'] ?>"
                    data-category="<?php echo $data['category'] ?>">
                <div class="flair <?php echo $data['flair_class2'] ?>"></div>
                <div><?php echo $data['display_name'] ?></div>
            </div>
        <?php endforeach; ?>
    </div>
</form>
<a href="#top" class="back-to-top">Back to top</a>
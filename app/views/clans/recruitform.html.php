<div class="banner" style="background-image:url('<?php img_src('banner.jpg') ?>')">
    <h1>/r/DestinyTheGame Discord</h1>
    <div class="banner__links">
        <a class="banner__link banner__link--subreddit" href="https://destinyreddit.com/" target="_blank">
            <span class="label">Subreddit</span>
            <svg class="hexagon" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 40 46">
                <g fill="#FFFFFF" fill-rule="nonzero">
                    <path d="M.474 34.873l19.058 11.003a.93.93 0 0 0 .936 0l19.058-11.003a.936.936 0 0 0 .467-.81V12.057a.936.936 0 0 0-.467-.81L20.468.244a.935.935 0 0 0-.936 0L.474 11.247a.935.935 0 0 0-.467.81v22.006c0 .334.178.643.467.81zm1.404-22.276L20 2.134l18.122 10.463v20.926L20 43.986 1.878 33.523V12.597z"/>
                </g>
            </svg>
            <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 24 24"><path style="fill:#FFFFFF;" d="M24 11.779c0-1.459-1.192-2.645-2.657-2.645-.715 0-1.363.286-1.84.746-1.81-1.191-4.259-1.949-6.971-2.046l1.483-4.669 4.016.941-.006.058c0 1.193.975 2.163 2.174 2.163 1.198 0 2.172-.97 2.172-2.163s-.975-2.164-2.172-2.164c-.92 0-1.704.574-2.021 1.379l-4.329-1.015c-.189-.046-.381.063-.44.249l-1.654 5.207c-2.838.034-5.409.798-7.3 2.025-.474-.438-1.103-.712-1.799-.712-1.465 0-2.656 1.187-2.656 2.646 0 .97.533 1.811 1.317 2.271-.052.282-.086.567-.086.857 0 3.911 4.808 7.093 10.719 7.093s10.72-3.182 10.72-7.093c0-.274-.029-.544-.075-.81.832-.447 1.405-1.312 1.405-2.318zm-17.224 1.816c0-.868.71-1.575 1.582-1.575.872 0 1.581.707 1.581 1.575s-.709 1.574-1.581 1.574-1.582-.706-1.582-1.574zm9.061 4.669c-.797.793-2.048 1.179-3.824 1.179l-.013-.003-.013.003c-1.777 0-3.028-.386-3.824-1.179-.145-.144-.145-.379 0-.523.145-.145.381-.145.526 0 .65.647 1.729.961 3.298.961l.013.003.013-.003c1.569 0 2.648-.315 3.298-.962.145-.145.381-.144.526 0 .145.145.145.379 0 .524zm-.189-3.095c-.872 0-1.581-.706-1.581-1.574 0-.868.709-1.575 1.581-1.575s1.581.707 1.581 1.575-.709 1.574-1.581 1.574z"/></svg>
        </a>
        <a class="banner__link banner__link--discord" href="https://destinyreddit.com/discord" target="_blank">
            <span class="label">Discord</span>
            <svg class="hexagon" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 40 46">
                <g fill="#FFFFFF" fill-rule="nonzero">
                    <path d="M.474 34.873l19.058 11.003a.93.93 0 0 0 .936 0l19.058-11.003a.936.936 0 0 0 .467-.81V12.057a.936.936 0 0 0-.467-.81L20.468.244a.935.935 0 0 0-.936 0L.474 11.247a.935.935 0 0 0-.467.81v22.006c0 .334.178.643.467.81zm1.404-22.276L20 2.134l18.122 10.463v20.926L20 43.986 1.878 33.523V12.597z"/>
                </g>
            </svg>
            <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 48 48"><path style="fill:#FFFFFF;" d="M 40 12 C 40 12 35.414063 8.410156 30 8 L 29.511719 8.976563 C 34.40625 10.175781 36.652344 11.890625 39 14 C 34.953125 11.933594 30.960938 10 24 10 C 17.039063 10 13.046875 11.933594 9 14 C 11.347656 11.890625 14.019531 9.984375 18.488281 8.976563 L 18 8 C 12.320313 8.535156 8 12 8 12 C 8 12 2.878906 19.425781 2 34 C 7.160156 39.953125 15 40 15 40 L 16.640625 37.816406 C 13.855469 36.847656 10.714844 35.121094 8 32 C 11.238281 34.449219 16.125 37 24 37 C 31.875 37 36.761719 34.449219 40 32 C 37.285156 35.121094 34.144531 36.847656 31.359375 37.816406 L 33 40 C 33 40 40.839844 39.953125 46 34 C 45.121094 19.425781 40 12 40 12 Z M 17.5 30 C 15.566406 30 14 28.210938 14 26 C 14 23.789063 15.566406 22 17.5 22 C 19.433594 22 21 23.789063 21 26 C 21 28.210938 19.433594 30 17.5 30 Z M 30.5 30 C 28.566406 30 27 28.210938 27 26 C 27 23.789063 28.566406 22 30.5 22 C 32.433594 22 34 23.789063 34 26 C 34 28.210938 32.433594 30 30.5 30 Z "/></svg>
        </a>
    </div>
</div>
<aside>
    <h3>Application rules</h3>
    <ul>
        <li>As clan leader or admin, you must be an active server member in good standing.
        Please keep in mind that your clan will also be held responsible for the clan leader/admin's
        actions in the Discord server. Please participate in a few discussions prior to posting and
        maintain your activity level in the Discord server.</li>
        <li>Do not post more than once every 7 days.</li>
        <li>Limit your clan descriptions to 400 characters with proper grammar and spelling.</li>
        <li>No low effort/low quality recruitment posts.</li>
        <li>The only acceptable links are Bungie.net clan links.</li>
    </ul>
    <small>Failure to adhere to the above rules may result in your inability to advertise on behalf of your
    clan for an extended period of time or the removal of your advertisement entirely.</small>
</aside>
<form id="ClanRecruitmentForm" method="POST" class="form <?php if (!discord_logged_in()): ?>not-logged-in<?php endif; ?>">
    <div class="form__head">
        <p>This form is for requesting your clan to be posted to the <code>#clan-recruitment</code>
        channel of the /r/DestinyTheGame Discord server.</p>
    </div>
    <section class="form__userlogin">
        <?php if (!discord_logged_in()): ?>
        <div class="field">
            <div class="field__head">
                <h3>You must identify your Discord account to submit the form.</h3>
            </div>
            <div class="field__input">
                <a class="discord-login-button"
                    href="<?php echo SITE_URL ?>discord-login">Confirm Your Discord</a>
            </div>
        </div>
        <?php else: ?>
        <div class="field" style="padding:8px 10px 5px">
            <h3>Welcome <?php xecho(get_discord_userping()) ?>
                <a class="discord-logout-button"
                    href="<?php echo SITE_URL ?>logout?cont=/clan-recruitment">Logout</a>
            </h3>
            <div class="field__input hide">
                <input name="discord_username" type="hidden"
                    value="<?php xecho(get_discord_username()) ?>" />
                <input name="discord_id" type="hidden"
                    value="<?php xecho(get_discord_id()) ?>" />
                <input name="discord_discriminator" type="hidden"
                    value="<?php xecho(get_discord_discriminator()) ?>" />
            </div>
        </div>
        <?php endif; ?>
    </section>
    <?php if (is_stashed('submitted') === true): ?>
    <section class="form__main">
        <div class="field">
            <p><?php echo u_stash('submit_response') ?></p>
        </div>
    </section>
    <?php else: ?>
    <section class="form__main" <?php if (!discord_logged_in()): ?>style="opacity:0.5"<?php endif; ?>>
        <?php if (b_stash('cursory_validate_failed')): ?>
        <div class="field" style="color:red">The form was not filled out correctly.</div>
        <?php endif; ?>
        <div class="field" data-for="bungie_profile">
            <div class="field__head">
                <h3>Please link us your own Bungie.net profile.</h3>
                <span>This must be your own Bungie.net profile (NOT your Bungie.net clan link).</span>
            </div>
            <div class="field__input">
                <input name="bungie_profile" type="url" <?php if (!discord_logged_in()) echo "disabled" ?> />
            </div>
            <div class="field__errors">
                <span class="field_error" data-errname="Required">This field is required</span>
                <span class="field_error" data-errname="BungieURL">Must be a Bungie.net URL.</span>
            </div>
        </div>
        <div class="field" data-for="clan_name">
            <div class="field__head">
                <h3>What is the name of your clan?</h3>
                <span>e.g. Reddit Discord</span>
            </div>
            <div class="field__input">
                <input name="clan_name" type="text" <?php if (!discord_logged_in()) echo "disabled" ?> />
            </div>
            <div class="field__errors">
                <span class="field_error" data-errname="Required">This field is required</span>
                <span class="field_error" data-errname="MaxLengthExceeded">Must be less than 50 characters.</span>
                <span class="field_error" data-errname="MinLengthNotMet">Must be at least 3 characters.</span>
            </div>
        </div>
        <div class="field" data-for="platform_option">
            <div class="field__head">
                <h3>What platform(s) will your clan allow applicants from?</h3>
            </div>
            <div class="field__input">
                <label for="platform_pc">
                    <input type="hidden" name="platform_pc" value="">
                    <input id="platform_pc" name="platform_pc" class="platform_option"
                            type="checkbox" value="PC"
                        <?php if (!discord_logged_in()) echo "disabled" ?>>
                    <span>PC</span>
                </label>
                <label for="platform_playstation">
                    <input type="hidden" name="platform_playstation" value="">
                    <input id="platform_playstation" name="platform_playstation" class="platform_option"
                            type="checkbox" value="Playstation"
                        <?php if (!discord_logged_in()) echo "disabled" ?>>
                    <span>Playstation</span>
                </label>
                <label for="platform_xbox">
                    <input type="hidden" name="platform_xbox" value="">
                    <input id="platform_xbox" name="platform_xbox" class="platform_option"
                            type="checkbox" value="Xbox"
                        <?php if (!discord_logged_in()) echo "disabled" ?>>
                    <span>Xbox</span>
                </label>
            </div>
            <div class="field__errors">
                <span class="field_error" data-errname="Required">Must selected at least one platform.</span>
            </div>
        </div>
        <div id="RegionSelector" class="field" data-for="RegionOption[]">
            <div class="field__head">
                <h3>What region(s) will your clan allow applicants from?</h3>
                <span>Tell us what region(s) your clan wants to recruit from:</span>
            </div>
            <div class="field__input">
                <label class="RegionOption no-select" data-value="north america">
                    <span>North America</span>
                    <input type="hidden" name="RegionOption[]" value="" />
                </label>
                <label class="RegionOption no-select" data-value="south america">
                    <span>South America</span>
                    <input type="hidden" name="RegionOption[]" value="" />
                </label>
                <label class="RegionOption no-select" data-value="europe">
                    <span>Europe</span>
                    <input type="hidden" name="RegionOption[]" value="" />
                </label>
                <label class="RegionOption no-select" data-value="middle east">
                    <span>Middle East</span>
                    <input type="hidden" name="RegionOption[]" value="" />
                </label>
                <label class="RegionOption no-select" data-value="oceania">
                    <span>Oceania</span>
                    <input type="hidden" name="RegionOption[]" value="" />
                </label>
                <label class="RegionOption no-select" data-value="asia">
                    <span>Asia</span>
                    <input type="hidden" name="RegionOption[]" value="" />
                </label>
                <label class="RegionOption no-select" data-value="africa">
                    <span>Africa</span>
                    <input type="hidden" name="RegionOption[]" value="" />
                </label>
                <label class="RegionSelectAll no-select">
                    <span>Select All</span>
                </label>
            </div>
            <div class="field__errors">
                <span class="field_error" data-errname="Required">Must select at least one region.</span>
            </div>
            <svg id="RegionSelector__map"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:amcharts="http://amcharts.com/ammap"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                version="1.1"
                viewBox="0 0 1020 660"
                width="40%"
                class="">
                <defs>
                    <style type="text/css">.land { fill: #CCCCCC; fill-opacity: 1; }</style>
                    <amcharts:ammap projection="mercator" leftLongitude="-169.6" topLatitude="83.68" rightLongitude="190.25" bottomLatitude="-55.55"></amcharts:ammap>
                </defs>
                <?php render_template('clans.recruitform-regionmap') ?>
            </svg>
        </div>
        <div class="field"  data-for="clan_desc">
            <div class="field__head">
                <h3>Briefly describe your clan.</h3>
                <span>Please limit your description to 400 characters with proper grammar and spelling.</span>
            </div>
            <div class="field__input">
                <textarea name="clan_desc" style="min-height:100px" maxlength="400"
                    <?php if (!discord_logged_in()) echo "disabled" ?>></textarea>
            </div>
            <div class="field__errors">
                <span class="field_error" data-errname="Required">This field is required</span>
                <span class="field_error" data-errname="MaxLengthExceeded">Must be less than 400 characters.</span>
                <span class="field_error" data-errname="MinLengthNotMet">Must be at least 25 characters.</span>
            </div>
        </div>
        <div class="field" data-for="clan_link">
            <div class="field__head">
                <h3>Please provide a link to your Bungie.net clan page.</h3>
                <span>e.g. https://www.bungie.net/en/ClanV2?groupid=2463957</span>
            </div>
            <div class="field__input">
                <input name="clan_link" type="url" <?php if (!discord_logged_in()) echo "disabled" ?> />
            </div>
            <div class="field__errors">
                <span class="field_error" data-errname="Required">This field is required</span>
                <span class="field_error" data-errname="BungieURL">Must be a Bungie.net clan link.</span>
            </div>
        </div>
        <?php if (discord_logged_in()): ?>
        <div class="form__bottom">
            <input type="submit" />
        </div>
        <?php endif; ?>
    </section>
    <?php endif; ?>
</form>

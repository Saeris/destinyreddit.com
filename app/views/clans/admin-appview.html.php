<?php
$app = xa_stash('app');
$app_id = $app['app_id'];
$data = $app['data'];
$clan = $data['clan'];
$d_user = $data['d_user'];
?>
<div class="App__view">
    <div class="App__head">
        <b>App #<?php xecho($app_id); ?>, on <?php xecho(timeConvert($app['submitted_utc'])); ?></b>
    </div>
    <div class="App__main">
        <div class="App__clanInfo">
            <h3>Clan</h3>
            <dl>
                <dt>ID</dt>
                <dd><?php xecho($clan['id']) ?></dd>

                <dt>Name</dt>
                <dd><?php xecho($clan['name']) ?></dd>

                <dt>Description</dt>
                <dd><?php xecho($clan['desc']) ?></dd>

                <dt>Link</dt>
                <dd><a href="<?php xecho($clan['link']) ?>"><?php xecho($clan['link']) ?></a></dd>

                <dt>Regions</dt>
                <dd><?php xecho($clan['regions']) ?></dd>

                <dt>Platforms</dt>
                <dd><?php xecho($clan['platforms']) ?></dd>

                <dt>Last Accepted App</dt>
                <dd><?php xecho(timeConvert($clan['app_last_submission']) ?: 'n/a') ?></dd>

                <dt>Clan Is Blacklisted</dt>
                <dd><?php xecho(bool_str($clan['is_blacklisted'])) ?></dd>

                <?php if (b_stash('is_form')): ?>
                <dt>Action:</dt>
                <dd>
                    <button type="submit" name="action" value="accept">Accept</button>
                    <button type="submit" name="action" value="deny">Deny</button>
                </dd>
                <?php elseif (!$app['in_acp']): ?>
                    <dt>Status</dt>
                    <dd>
                        <?php xecho($app['accepted'] ? 'Accepted' : 'Denied'); ?>
                        by <b><?php xecho($app['acp_user'] ?? 'n/a'); ?></b>
                        on <b><?php xecho(timeConvert($app['acp_time']) ?? 'n/a'); ?></b>
                    </dd>

                    <dt>Status Reason</dt>
                    <dd><?php xecho($app['acp_reason'] ?? 'n/a'); ?></dd>
                <?php endif; ?>
            </dl>
        </div>
        <div class="App__duserInfo">
            <h3>Discord User</h3>
            <dl>
                <dt>ID</dt>
                <dd><?php xecho($d_user['id']) ?></dd>

                <dt>User Ping</dt>
                <dd><?php xecho($d_user['userping']) ?></dd>

                <dt>Bungie Profile</dt>
                <dd><a href="<?php xecho($d_user['bungie_profile']) ?>"><?php
                        xecho($d_user['bungie_profile']) ?></a></dd>

                <dt>User Is Blacklisted</dt>
                <dd><?php xecho(bool_str($d_user['is_blacklisted'])) ?></dd>

                <dt>Activity 7d</dt>
                <dd><?php xecho($d_user['activity_7d']) ?> messages</dd>
            </dl>
            <h3>User Actioned</h3>
            <dl>
                <dt>Ban</dt>
                <dd><?php xecho($d_user['actioned']['Ban']) ?></dd>
                <dt>Unban</dt>
                <dd><?php xecho($d_user['actioned']['Unban']) ?></dd>
                <dt>Warn</dt>
                <dd><?php xecho($d_user['actioned']['Warn']) ?></dd>
                <dt>Note</dt>
                <dd><?php xecho($d_user['actioned']['Note']) ?></dd>
                <dt>Mute</dt>
                <dd><?php xecho($d_user['actioned']['Mute']) ?></dd>
                <dt>Total Infractions<br/>(Ban, Warn, Mute)</dt>
                <dd><?php xecho($d_user['actioned']['Infractions']) ?></dd>
            </dl>
        </div>
    </div>
</div>
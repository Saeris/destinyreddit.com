<aside>
    <?php if (u_stash('page') == '403'): ?>
    <h1>&nbsp;</h1>
    <?php else: ?>
    <h1>Clan-Recruitment ACP</h1>
    <h3>Navigation</h3>
    <ul>
        <li><a href="<?php echo SITE_URL ?>clan-recruitment/admin"
            class="<?php if (u_stash('page') == 'home') echo 'selected' ?>">ACP Home</a></li>
        <li><a href="<?php echo SITE_URL ?>clan-recruitment/admin/queue"
            class="<?php if (u_stash('page') == 'queue') echo 'selected' ?>">Manual Queue</a></li>
        <li><a href="<?php echo SITE_URL ?>clan-recruitment/admin/blacklist"
            class="<?php if (u_stash('page') == 'blacklist') echo 'selected' ?>">Blacklist</a></li>
        <li><a href="<?php echo SITE_URL ?>clan-recruitment/admin/history"
            class="<?php if (u_stash('page') == 'history') echo 'selected' ?>">History</a></li>
        <li><a href="<?php echo SITE_URL ?>logout?cont=/clan-recruitment/admin">Logout</a></li>
    </ul>
    <?php endif; ?>
</aside>
<article>
    <h2><?php echo x_stash('acp_title') ?></h2>
<?php if (u_stash('page') == 'home'): ?>
    <p>Welcome, <?php xecho(get_discord_userping()); ?>.</p>
    <p style="margin-top:10px">Select an option from the navigation.</p>
<?php elseif (u_stash('page') == 'blacklist'): ?>
    <form class="blacklist_add" method="POST" style="margin-bottom:20px;">
        <input type="hidden" name="action" value="add" />
        <fieldset>
            <legend>Add To Blacklist</legend>
            <select name="fieldtype" required>
                <option value="clan_id">Clan ID</option>
                <option value="d_user_id">Discord User ID</option>
            </select>
            <input type="text" name="fieldval" value="" placeholder="Value" required />
            <input type="submit" value="Add" />
        </fieldset>
    </form>
    <h3>Blacklist Table</h3>
    <div class="blacklist_table">
        <div class="row">
            <span class="fieldtype">Type</span>
            <span class="fieldval">Value</span>
            <span class="blacklisted_utc">Date added (UTC)</span>
            <span class="blacklisted_by">Added by</span>
            <span class="blacklisted_by">Attempts</span>
        </div>
        <?php foreach (xa_stash('blacklist') as $item): ?>
        <form class="row" method="POST">
            <input type="hidden" name="action" value="remove" />
            <input type="hidden" name="id" value="<?php xecho($item['id']) ?>" />
            <input type="hidden" name="fieldtype" value="<?php xecho($item['fieldtype']) ?>" />
            <input type="hidden" name="fieldval" value="<?php xecho($item['fieldval']) ?>" />
            <?php if ($item['fieldtype'] == 'clan_id'): ?>
                <span class="fieldtype">Clan ID</span>
                <span class="fieldval"><?php xecho($item['fieldval']) ?></span>
            <?php elseif ($item['fieldtype'] == 'd_user_id'): ?>
                <span class="fieldtype">Discord User ID</span>
                <span class="fieldval"><?php xecho($item['fieldval']) ?></span>
            <?php endif; ?>
            <span class="blacklisted_utc"><?php echo timeConvert($item['blacklisted_utc']) ?></span>
            <span class="blacklisted_by"><?php xecho($item['blacklisted_by']) ?></span>
            <span class="attempts"><?php xecho($item['attempts']) ?></span>
            <input type="submit" value="Remove from blacklist" />
        </form>
        <?php endforeach; ?>
    </div>
<?php elseif (u_stash('page') == 'queue'): ?>
    <?php if (empty(u_stash('apps'))): ?>
    <div style="display:flex;justify-content:center;">
        <img src="<?php img_src('queue_kitteh.png'); ?>" />
    </div>
    <?php else: ?>
    <div class="App__manualQueue">
        <?php foreach (u_stash('apps') as $app): ?>
        <form method="POST">
            <input type="hidden" name="app_id" value="<?php xecho($app['app_id']); ?>" />
            <?php render_template('clans.admin-appview', array(
                'app'       => $app,
                'is_form'   => true,
            )); ?>
        </form>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
<?php elseif (u_stash('page') == 'history'): ?>
    <div class="App__history">
        <div class="App__historyNav">
            <p>Page <?php echo x_stash('page_number') ?> of <?php echo x_stash('page_count') ?></p>
            <?php if (i_stash('page_number') > 1): ?>
                <a href="?page=<?php echo (i_stash('page_number') - 1) ?>"><button>prev</button></a>
            <?php endif; ?>
            <?php if (i_stash('page_number') < i_stash('page_count')): ?>
                <a href="?page=<?php echo (i_stash('page_number') + 1) ?>"><button>next</button></a>
            <?php endif; ?>
        </div>
        <?php foreach (u_stash('listing') as $app): ?>
        <?php render_template('clans.admin-appview', array(
            'app'       => $app,
            'is_form'   => false,
        )); ?>
        <?php endforeach; ?>
    </div>
<?php elseif (u_stash('page') == '403'): ?>
    <?php if (discord_logged_in()): ?>
    <p>You do not have permission to access this page.</p>
    <?php else: ?>
    <p>You must be logged in to access this page.</p>
    <a href="<?php echo SITE_URL ?>discord-login?cont=<?php xecho(rawurlencode(current_url())) ?>">Login with Discord</a>
    <?php endif; ?>
<?php elseif (u_stash('page') == '404'): ?>
    <p>404 not found.</p>
<?php endif; ?>
</article>
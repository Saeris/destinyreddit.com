<?php
/*
routes.php
~~~~~~~~~~
# -> runs controller
% -> runs string as PHP code
*/

App::router()

    ->GET('/',                                  '%redirect_reddit("/r/DestinyTheGame")')
    ->GET('/discord',                           '%redirect_ext("https://discord.gg/DestinyReddit")')

    // /r/DestinyTheGame Flair Selector
    // --------------------------------------------------------------------------------
    ->GET('/flair',                             '#FlairSelector')
    ->POST('/flair',                            '#FlairSelector:submit')
    ->GET('/flair.json',                        '#FlairSelector:json')
    ->GET('/admin/stylesheet',                  '#Stylesheet:page')
    ->ANY('/admin/stylesheet/{action}',         '#Stylesheet:action')
    ->GET('/admin/{name}',                      '#Admin')

    // DTG Discord #clan-recruitment form
    // --------------------------------------------------------------------------------
    ->GET('/clan-recruitment',                  '#ClanRecruitment')
    ->POST('/clan-recruitment',                 '#ClanRecruitment:submit')
    ->GET('/clan-recruitment/admin[/{page}]',   '#ClanRecruitment:admin')
    ->POST('/clan-recruitment/admin[/{page}]',  '#ClanRecruitment:admin')
    ->GET('/discord-login',                     '#DiscordLogin:login')

    // Login / Logout
    // --------------------------------------------------------------------------------
    ->GET('/login',                             '#Login:login')
    ->GET('/logout',                            '#Login:logout')

;
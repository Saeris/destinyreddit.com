<?php
date_default_timezone_set('Europe/London');

define('SITE_ADMINS', ['kwwxis', 'Clarkey7163', 'D0cR3d', 'DTG_Bot', 'K_Lobstah',
    'Fuzzle_hc', 'Squeagley', 'MikeyJayRaymond', 'MisterWoodhouse', 'spaghetticatt',
    'notliam', 'NorseFenrir', 'aaron-il-mentor', 'MetalGilSolid', 'redka243', 'The--Marf',
    'thirdegree', 'MattyMcD', 'Hawkmoona_Matata', 'OxboxturnoffO', 'GenericDreadHead',
    'RiseOfBacon', 'Ruley9', 'Tahryl', 'ZarathustraEck', 'GreenLego', 'aslak1899',
    'Try_to_guess_my_psn', 'LucentBeam8MP', 'irJustineee']);
define('CSS_VERSION', '1.0.9');
define('JS_VERSION', '1.2.10');

require APP_ROOT.'lib/session.php';

// HELPERS
// --------------------------------------------------------------------------------
function db($dbname) {
    return App::db($dbname);
}

// USER
// --------------------------------------------------------------------------------
function client() {
    return \app\lib\RedditClient::get_client();
}

function logged_in() {
    return \app\lib\RedditClient::logged_in();
}

function get_username() {
    return \app\lib\RedditClient::get_username();
}

function is_admin() {
    return in_array(get_username(), SITE_ADMINS);
}

function discord_client() {
    return \app\lib\DiscordClient::get_client();
}

function discord_logged_in() {
    return \app\lib\DiscordClient::logged_in();
}

function get_discord_username() {
    return \app\lib\DiscordClient::get_username();
}

function get_discord_userping() {
    return '@' . get_discord_username() . '#' . get_discord_discriminator();
}

function get_discord_id() {
    return \app\lib\DiscordClient::get_id();
}

function get_discord_discriminator() {
    return \app\lib\DiscordClient::get_discriminator();
}

function discord_is_admin() {
    if (!discord_logged_in()) {
        return false;
    }
    if (isset($_SESSION['d_is_admin'])) {
        return $_SESSION['d_is_admin'];
    }
    return $_SESSION['d_is_admin'] = (true === db('SweeperBot')->queryFirstField(
        'SELECT is_mod FROM "Users" WHERE "UserID"=%s_usrid AND "ServerID"=%s_srvid',
        [
            'usrid' => get_discord_id(),
            'srvid' => DISCORD_GUILD_ID
        ]
    ));
}

// START
// --------------------------------------------------------------------------------
session_init();

\View::addProfile('net.layout', function($view) {
    $view->virtual_script(LAYER7_SITE_NAME, 'ProductionClient', LAYER7_SITE_JS . 'ProductionClient.js?v=1.4.4');
    $view->usingLib('roboto_font');
    $view->stylesheets('main');
    $view->favicontype('png');
});

return 'profile/routes.php';
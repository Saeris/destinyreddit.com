<?php
namespace app\lib;

class RedditClient {

    public static function logged_in() {
        return isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true;
    }

    public static function get_username() {
        return from($_SESSION, 'r_username');
    }

    public static function switchSession($username, $cont=null) {
        session_regenerate_id();

        // set auth session variables
        $_SESSION['r_username'] = $username;
        $_SESSION['logged_in']  = true;
        $_SESSION['token'] = base64_encode(openssl_random_pseudo_bytes(32));

        // redirect to overview or 'cont' if set
        redirect(isset($cont) ? $cont : 'flair');
        die();
    }

    public static function failure($error = null) {
        if ($error == 'access_denied') {
            redirect_cont();
        }

        view('net.loginfailed')
            ->meta('Login failed', 'page--login_failed')
            ->stylesheets('inlogin')
            ->use('error', $error)
            ->dispatch();
        die();
    }

    public static function authorize() {
        autoload_oauth2_client();

        // Instantiate the OAuth2 Client
        $client = new \OAuth2Client\Client(
            OAUTH_CLIENT_ID,
            OAUTH_CLIENT_SECRET,
            \OAuth2Client\Client::AUTH_TYPE_AUTHORIZATION_BASIC,
            CACERT_FILE
        );
        $client->setCurlOption(CURLOPT_USERAGENT, OAUTH_USER_AGENT);

        if (isset($_REQUEST['error'])) {
            // Oh snap, an error (or the user declined to authorize)!
            self::failure($_REQUEST['error']);
        }

        // Save our 'continue' URL from REQUEST to the SESSION
        if (isset($_REQUEST['cont'])) {
            $_SESSION['r_cont_after_login'] = $_REQUEST['cont'];
        }

        // Check if we have an access_token,
        //   If we already have it, then that means our user is already logged in
        //   If we don't have it, then let's log them in
        if (!isset($_SESSION['access_token'])) {
            // Check if we have our code
            if (!isset($_GET["code"])) {
                // If we don't have it then we need to redirect the user to Reddit's
                //    "are you sure you want to give Blah blah access to your reddit account?" page
                // If they choose yes, then the user is redirected back to here with a working code
                // If they choose no, then the user is redirected back to here with the 'error' parameter

                $auth_url = $client->getAuthenticationUrl(
                    OAUTH_AUTHORIZE_URL,
                    OAUTH_REDIRECT_URL,
                    array(
                        "scope"     => "identity",
                        "state"     => safe_token(),
                        "duration"  => "permanent"
                    )
                );
                redirect_ext($auth_url);
            } else if (isset($_GET["code"])) {
                // We have our code, let's get an access & refresh
                // token, then contact Reddit's API

                // First get the access & refresh token
                $response = $client->retrieveAccessToken(
                    OAUTH_ACCESS_TOKEN_URL,
                    "authorization_code",
                    array(
                        "code" => $_GET["code"],
                        "redirect_uri" => OAUTH_REDIRECT_URL
                    )
                );

                $accessTokenResult = $response["result"];

                if (!isset($accessTokenResult["access_token"])) {
                    self::failure("unable to retrieve access token");
                }

                $client->setAccessToken($accessTokenResult["access_token"]);

                $_SESSION['access_token']  = $accessTokenResult["access_token"];
                $_SESSION['refresh_token'] = $accessTokenResult["refresh_token"];

                $client->setAccessTokenType(\OAuth2Client\Client::ACCESS_TOKEN_BEARER);

                // Now let's try to get the Reddit user's username
                $userdata = $client->fetch("https://oauth.reddit.com/api/v1/me.json");

                if (!isset($userdata['result']['name'])) {
                    self::failure("we were unable to retrieve your username from the Reddit API");
                }

                // --------------------------------------------------------------------------------
                // We're done with OAuth and Reddit API stuff, and everything checked out!
                // Let's configure our user's session and redirect them

                self::switchSession($userdata['result']['name'], from($_SESSION, 'r_cont_after_login'), true );
            } else {
                echo "Authorization failed.";
            }

        } else {
            if (isset($_SESSION['r_cont_after_login'])) {
                $cont = $_SESSION['r_cont_after_login'];
                unset($_SESSION['r_cont_after_login']);
                redirect($cont);
            } else {
                redirect('/flair');
            }
            die();
        }
    }

    public static function get_client() {
        static $_client = null;

        if ($_client !== null) {
            return $_client;
        }

        if (!isset($_SESSION['logged_in']) || $_SESSION['logged_in'] !== true) {
            return null;
        }

        $response = self::refreshToken();
        return $_client = $response['client'];
    }

    public static function refreshToken() {
        if (!empty($_SESSION['refresh_token'])) {
            autoload_oauth2_client();

            $params = array("refresh_token" => $_SESSION['refresh_token']);

            $client = new \OAuth2Client\Client(
                OAUTH_CLIENT_ID,
                OAUTH_CLIENT_SECRET,
                \OAuth2Client\Client::AUTH_TYPE_AUTHORIZATION_BASIC,
                CACERT_FILE
            );

            $client->setCurlOption(CURLOPT_USERAGENT, OAUTH_USER_AGENT);
            $response = $client->retrieveAccessToken(OAUTH_ACCESS_TOKEN_URL, 'refresh_token', $params);

            $client->setAccessToken($response["result"]["access_token"]);

            $_SESSION['access_token'] = $response["result"]["access_token"];

            $client->setAccessTokenType(\OAuth2Client\Client::ACCESS_TOKEN_BEARER);

            return array('response' => $response, 'client' => $client);
        } else {
            session_destroy();
            echo '<html><head></head><body>Internal Error: your session was lost for reasons unknown.&nbsp;' .
                '<a href="'.SITE_URL.'login">Click here to log back in.</a></body></html>';
            die();
        }
    }
}
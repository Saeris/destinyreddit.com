<?php
namespace app\lib\clans;

class Blacklist {

    public static function duser_is_blacklisted($discord_id, $attempt = false) {
        return self::is_blacklisted('d_user_id', $discord_id, $attempt);
    }

    public static function clan_is_blacklisted($clan_id, $attempt = false) {
        return self::is_blacklisted('clan_id', $clan_id, $attempt);
    }

    public static function get_blacklisted() {
        return db('SweeperBot')->query(
                    'SELECT * FROM "ClanRecruitmentBlacklist" WHERE unblacklisted=false ORDER BY blacklisted_utc DESC');
    }

    public static function is_blacklisted($fieldtype, $fieldval, $attempt = false) {
        if (empty($fieldtype) || empty($fieldval)) {
            return false;
        }

        $row = db('SweeperBot')->queryFirstRow(
                    'SELECT "id", "attempts" FROM "ClanRecruitmentBlacklist"
                    WHERE (fieldtype=%s AND fieldval=%s) AND unblacklisted=false',
                        $fieldtype, $fieldval);

        if (empty($row)) {
            return false;
        }

        if ($attempt) {
            db('SweeperBot')->update('ClanRecruitmentBlacklist', array(
                'attempts' => $row['attempts'] + 1,
            ), '"id"=%s', $row['id']);
        }

        return true;
    }

    public static function add_to_blacklist($fieldtype, $fieldval) {
        if (empty($fieldtype) || empty($fieldval)) {
            return false;
        }
        db('SweeperBot')->insertUpdate('ClanRecruitmentBlacklist', array(
            'fieldtype'         => $fieldtype,
            'fieldval'          => $fieldval,
            'blacklisted_utc'   => time(),
            'blacklisted_by'    => get_discord_userping(),
            'unblacklisted'     => false,
        ));
    }

    public static function remove_from_blacklist($id) {
        if (empty($id)) {
            return false;
        }
        db('SweeperBot')->update('ClanRecruitmentBlacklist', array(
            'unblacklisted'     => true,
            'unblacklisted_by'  => get_discord_userping(),
            'unblacklisted_utc' => time(),
        ), '"id"=%s', $id);
    }

}
<?php
namespace app\lib\clans;

use app\lib\DiscordMod;

class RecruitForm {
    const FIELDS = [
        'bungie_profile',
        'clan_name',
        'platform_pc',
        'platform_playstation',
        'platform_xbox',
        'RegionOption',
        'clan_desc',
        'clan_link',
    ];

    public static function cursory_validate($data) {
        if (!discord_logged_in()) {
            return false;
        }

        // ----- VALIDATE
        $data = array_intersect_key($data, array_flip(self::FIELDS));
        if (count($data) !== count(self::FIELDS)) {
            return false;
        }

        if (empty($data['bungie_profile']) || empty($data['clan_name'])
            || empty($data['clan_desc']) || empty($data['clan_link'])) {
            return false;
        }

        $platforms = array_filter([$data['platform_pc'], $data['platform_playstation'], $data['platform_xbox']]);
        if (empty($platforms)) {
            return false;
        } else {
            $data['platforms'] = trim(implode(', ', $platforms));
            unset($data['platform_pc']);
            unset($data['platform_playstation']);
            unset($data['platform_xbox']);
        }

        if (strlen($data['clan_name']) > 50 || strlen($data['clan_name']) < 3) {
            return false;
        }

        if (strlen($data['clan_desc']) > 400 || strlen($data['clan_desc']) < 25) {
            return false;
        }

        $data['RegionOption'] = array_filter($data['RegionOption']);
        if (empty($data['RegionOption'])) {
            return false;
        } if (count($data['RegionOption']) == 7) {
            $data['RegionOption'] = 'Global (all regions)';
        } else {
            $data['RegionOption'] = ucwords(trim(implode(', ', $data['RegionOption'])));
        }

        if (!in_array(full_domain(strtolower($data['bungie_profile'])), ['bungie.net', 'www.bungie.net'])) {
            return false;
        }
        if (!in_array(full_domain(strtolower($data['clan_link'])), ['bungie.net', 'www.bungie.net'])) {
            return false;
        }

        $clan_link_test = explode('bungie.net', strtolower($data['clan_link']), 2);
        if (count($clan_link_test) < 2) {
            return false;
        } else {
            $clan_link_test = $clan_link_test[1];
        }
        if (!startsWith($clan_link_test, '/en/clanv2?groupid=') &&
            !startsWith($clan_link_test, '/en/clanv2/chat?groupid=')) {
            return false;
        } else {
            $clan_id = explode('groupid=', $clan_link_test, 2);
            if (count($clan_id) < 2) {
                return false;
            }
            $clan_id = preg_split('/[^0-9]/', $clan_id[1])[0];
            if (empty($clan_id)) {
                return false;
            }
            $data['clan_id'] = $clan_id;
        }

        // ----- EXPAND DATA
        return [
            'clan' => [
                'name'      => trim($data['clan_name']),
                'id'        => $data['clan_id'],
                'desc'      => trim($data['clan_desc']),
                'link'      => trim($data['clan_link']),
                'regions'   => $data['RegionOption'],
                'platforms' => $data['platforms'],

                'app_is_acp_pending' =>
                    0 !== db('SweeperBot')->queryFirstField(
                    'SELECT count(*) FROM "ClanRecruitmentApps" WHERE in_acp=true AND clan_id=%s',
                        $data['clan_id']),

                'app_last_submission' =>
                    db('SweeperBot')->queryFirstRow(
                    'SELECT * FROM "ClanRecruitmentApps" WHERE clan_id=%s AND accepted=true
                    ORDER BY submitted_utc DESC LIMIT 1',
                        $data['clan_id'])['submitted_utc'] ?? null,

                'is_blacklisted' => Blacklist::clan_is_blacklisted($data['clan_id'], true),
            ],
            'd_user' => [
                'id'                => get_discord_id(),
                'discriminator'     => get_discord_discriminator(),
                'username'          => get_discord_username(),
                'userping'          => get_discord_userping(),
                'bungie_profile'    => $data['bungie_profile'],

                'is_blacklisted' => Blacklist::duser_is_blacklisted(get_discord_id(), true),

                'activity_7d' =>
                    db('SweeperBot')->queryFirstField(
                    "SELECT COUNT(*) FROM message_data
                    WHERE userid=%s AND serverid=%s
                    AND channelid NOT IN (
                        '169428490091757568',
                        '354627968770768896',
                        '354628278088237056',
                        '369352134807912448',
                        '369354772786184202',
                        '235123818803363841',
                        '235123673181192192'
                    )
                    AND msgcreated > (current_date - interval '7 days')",
                        get_discord_id(),
                        DISCORD_GUILD_ID),

                'actioned' => array_reduce(
                    db('SweeperBot')->query(
                        'SELECT "ActionType", COUNT(*) FROM "ModActions"
                        WHERE "UserID"=%s AND "ServerID"=%s AND
                        "ActionType"!=\'Note\' GROUP BY "ActionType"',
                            get_discord_id(), DISCORD_GUILD_ID),
                    function($acc, $row) {
                        $acc[$row['ActionType']] = $row['count'];
                        if (in_array($row['ActionType'], ['Ban', 'Warn', 'Mute'])) {
                            $acc['Infractions'] += $row['count'];
                        }
                        return $acc;
                    },
                    [
                        'Ban' => 0,
                        'Unban' => 0,
                        'Warn' => 0,
                        'Note' => 0,
                        'Mute' => 0,
                        'Infractions' => 0,
                    ]
                ),
            ]
        ];
    }

    public static function process($data) {
        if ($data === false) {
            return false;
        }
        db('SweeperBot')->insert('ClanRecruitmentApps', [
            'data'          => json_encode($data),
            'submitted_utc' => time(),
            'clan_id'       => $data['clan']['id'],
            'd_user_id'     => $data['d_user']['id'],
            'in_acp'        => false,
            'accepted'      => false,
        ]);

        $app_id = db('SweeperBot')->insertId();

        // ----- Check if pending manual review
        if ($data['clan']['app_is_acp_pending']) {
            self::submit_to_denied($app_id, 'Automatic validation failed: clan has pending app');
            return "Your clan already has a recruitment application pending moderator review.";
        }

        // ----- Check d_user activity
        if (!discord_is_admin() && ($data['d_user']['activity_7d'] < 25)) {
            // require at least 25 messages in past 7 days
            self::submit_to_denied($app_id, 'Automatic validation failed: failed activity requirement (25 messages in past 7d)');
            return "<p>Sorry, but you do not meet the activity requirements.</p>" .
                  "<p>An active member is someone who participates in discussions with other users, ideally multiple times a week. We aren’t going to give specifics, but the requirement isn’t difficult to meet. We encourage you to interact with and get to know some of the other folks around here. Engage in a handful of conversations over text over the next few days outside of LFG & statsville.</p>" .
                  "<p>We don’t count LFG as we consider people to be active server members when they participate in discussions within the server, not just as an LFG site. Statsville is interacting with a bot rather than other users which is why it isn't considered. We do not count voice activity as there's not a reliable way to qualify/quantify those interactions.</p>" .
                  "<p>This activity ruling is put in place to ensure people are not just joining to use our resources and abuse them. If you have questions please DM @SweeperBot#6175.</p>";
        }

        // ----- Check blacklist based on d_user.id or clan.id
        if ($data['d_user']['is_blacklisted'] || $data['clan']['is_blacklisted']) {
            self::submit_to_denied($app_id, 'Automatic validation failed: ' .
                ($data['d_user']['is_blacklisted'] ? 'discord user' : 'clan') . ' is blacklisted');
            return "Sorry, but your clan is not allowed to be posted. " .
                   "If you have questions please DM @SweeperBot#6175.";
        }

        // ----- Check rate limit (1 posting per 7 days)
        if (!discord_is_admin() && (time() - ($data['clan']['app_last_submission'] ?? 0)) < 604800) {
            self::submit_to_denied($app_id, 'Automatic validation failed: hit rate limit (1 posting per 7d)');
            return "Your clan recruitment application has been declined. " .
                   "We allow only 1 posting max per 7 days.";
        }

        // ----- Check if d_user has Warnings/Mutes/Bans
        if (true || $data['d_user']['actioned']['Infractions'] > 0) {
            self::submit_to_acp($app_id);
            return "Your clan recruitment application has been directed to manual approval. " .
                   "Please wait for a moderator to review your application";
        }
        // ----- Accept
        else {
            self::submit_to_channel($app_id);
            return "Your clan recruitment application has been accepted.";
        }
    }

    public static function submit_to_acp($app_id) {
        db('SweeperBot')->update('ClanRecruitmentApps', array(
            'in_acp' => true,
        ), "app_id=%s", $app_id);
        DiscordMod::post_message(DISCORD_MOD_ONLY_CHANNEL_ID,
            "There's a new item in the clan-recruitment manual queue!\n" .
            "<https://destinyreddit.com/clan-recruitment/admin/queue>");
    }

    public static function submit_to_denied($app_id, $acp_reason = null, $acp_user = null) {
        db('SweeperBot')->update('ClanRecruitmentApps', array(
            'accepted'      => false,
            'in_acp'        => false,
            'acp_user'      => $acp_user ?? '@SweeperBot#6175',
            'acp_time'      => time(),
            'acp_reason'    => $acp_reason ?? 'Automatic validation failed.',
        ), "app_id=%s", $app_id);
    }

    public static function submit_to_channel($app_id, $acp_reason = null, $acp_user = null) {
        $data = json_decode(db('SweeperBot')->queryFirstField(
                    'SELECT "data" FROM "ClanRecruitmentApps"
                    WHERE app_id=%s', $app_id) ?: '{}', true);
        if (empty($data)) return false;

        db('SweeperBot')->update('ClanRecruitmentApps', array(
            'accepted'      => true,
            'in_acp'        => false,
            'acp_user'      => $acp_user ?? '@SweeperBot#6175 ',
            'acp_time'      => time(),
            'acp_reason'    => $acp_reason ?? 'Automatic validation passed.'
        ), "app_id=%s", $app_id);

        $message = '';

        $message .= "**Name:** {$data['clan']['name']}\n";
        $message .= "**Contact:** <@{$data['d_user']['id']}>\n";
        $message .= "**Platforms:** {$data['clan']['platforms']}\n";
        $message .= "**Region:** {$data['clan']['regions']}\n";
        $message .= "**Description:** {$data['clan']['desc']}\n";
        $message .= "**Link:** <{$data['clan']['link']}>\n";
        $message .= "<:blank:364964889770590208>";

        DiscordMod::post_message(DISCORD_CLAN_RECRUITMENT_CHANNEL_ID, $message);
    }

}
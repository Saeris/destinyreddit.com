<?php
namespace app\lib\clans;

class ManualQueue {

    public static function row_pass($row) {
        $row['data'] = json_decode($row['data'], true);
        if ($row['in_acp']) {
            $row['data']['clan']['is_blacklisted'] = Blacklist::clan_is_blacklisted($row['data']['clan']['id']);
            $row['data']['d_user']['is_blacklisted'] = Blacklist::duser_is_blacklisted($row['data']['d_user']['id']);
        }
        return $row;
    }

    public static function get_apps() {
        return array_map('self::row_pass',
                db('SweeperBot')->query('SELECT * FROM "ClanRecruitmentApps" WHERE in_acp=true'));
    }

    public static function get_history($page = 1, $max_items = 25) {
        $page = max(intval($page), 1);
        $max_items = min(intval($max_items), 100);

        $page_count = intval(ceil((db('SweeperBot')->queryFirstField(
                    'SELECT count(*) FROM "ClanRecruitmentApps" WHERE in_acp=false') ?? 0) / $max_items));

        $page = min($page, $page_count);

        return array(
            'listing' => array_map('self::row_pass',
                db('SweeperBot')->query(
                    'SELECT * FROM "ClanRecruitmentApps"
                    WHERE in_acp=false
                    ORDER BY submitted_utc DESC
                    LIMIT %i OFFSET %i',
                        $max_items, ($page - 1) * $max_items)),
            'page_number' => $page,
            'page_count'  => $page_count,
        );
    }

}
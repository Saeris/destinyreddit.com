<?php
namespace app\lib;

class RedditMod {
    private static $client = null;

    public static function get_client() {
        if (isset(self::$client)) {
            return self::$client;
        }

        autoload_oauth2_client(['Password']);

        self::$client = new \OAuth2Client\Client(
            OAUTH_MODERATOR_ID,
            OAUTH_MODERATOR_SECRET,
            \OAuth2Client\Client::AUTH_TYPE_FORM,
            CACERT_FILE
        );
        self::$client->setCurlOption(CURLOPT_USERAGENT, OAUTH_MODERATOR_USER_AGENT);

        self::establishAccess();

        return self::$client;
    }

    private static function establishAccess() {
        if (!cached_available())
            die('memcached not available');

        $cached_key = 'dtg_modtoken';
        $cached_exp = 60 * 59; // expire in 60 minutes
        $cached_val = cached($cached_key);

        if (empty($cached_val)) {
            list($token_type, $access_token) = self::requestRefreshToken();
            cached($cached_key, ($token_type . ':' . $access_token), $cached_exp);
        } else {
            list($token_type, $access_token) = explode(':', $cached_val, 2);
        }

        self::$client->setAccessToken($access_token);
        self::$client->setAccessTokenType($token_type);
    }

    private static function requestRefreshToken() {
        $query = array(
            'client_id'         => OAUTH_MODERATOR_ID,
            'response_type'     => 'code',
            'state'             => safe_token(),
            'redirect_uri'      => OAUTH_MODERATOR_REDIRECT_URL,
            'duration'          => 'permanent',
            'scope'             => 'save,modposts,identity,edit,flair,history,modconfig,'.
                                   'modflair,modlog,modposts,modwiki,mysubreddits,privatemessages,'.
                                   'read,report,submit,subscribe,vote,wikiedit,wikiread'
        );
        $grant_type = 'password';
        $post_data = array(
            'username'          => OAUTH_MODERATOR_USERNAME,
            'password'          => OAUTH_MODERATOR_PASSWORD,
        );
        $headers = array(
            'Authorization' => 'Basic ' . base64_encode(OAUTH_MODERATOR_ID . ':' . OAUTH_MODERATOR_SECRET)
        );

        $response = self::$client->retrieveAccessToken(
            OAUTH_ACCESS_TOKEN_URL . '?' . http_build_query($query, null, '&'),
            $grant_type,
            $post_data,
            $headers
        );

        return array(
            \OAuth2Client\Client::ACCESS_TOKEN_BEARER,
            $response["result"]["access_token"],
        );
    }
}
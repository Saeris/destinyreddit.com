<?php
namespace app\lib\flair;

use app\lib\RedditMod;
use app\lib\flair\FlairConfig;
use app\lib\flair\FlairChangeResult;

class FlairManager {
    const SUBREDDIT     = "DestinyTheGame";
    const API_ENDPOINT  = array(
        'SET_FLAIR' => "https://oauth.reddit.com/r/%s/api/flair",
        'STYLESHEET' => "https://oauth.reddit.com/r/%s/about/stylesheet",
    );
    const CACHED_IMAGES_KEY = 'dtg_images';
    const CACHED_IMAGES_EXP = 60 * 60 * 24 * 7; // 7 days

    private static function get_endpoint($name) {
        return sprintf(self::API_ENDPOINT[$name], self::SUBREDDIT);
    }

    // stylesheet images
    // --------------------------------------------------------------------------------
    public static function force_stylesheet_images_cache_expiry() {
        cached(self::CACHED_IMAGES_KEY, '');
    }

    public static function get_stylesheet_images() {
        $cached_val = cached(self::CACHED_IMAGES_KEY);

        if (empty($cached_val)) {
            $res = RedditMod::get_client()->fetch(self::get_endpoint('STYLESHEET'));
            $res = $res['result']['data']['images'];

            $image_map = [];
            foreach ($res as $item) {
                $image_map[$item['name']] = $item;
            }

            cached(self::CACHED_IMAGES_KEY, serialize($image_map), self::CACHED_IMAGES_EXP);
            return $image_map;
        } else {
            return unserialize($cached_val);
        }
    }

    public static function get_stylesheet_image_urls() {
        return array_filter(array_map(function($item) {
            return $item['url'] ?? null;
        }, array_values(self::get_stylesheet_images())));
    }

    // validators
    // --------------------------------------------------------------------------------
    public static function isValidFlairClass($css_class) {
        if (empty($css_class) || !FlairConfig::is_valid_class($css_class)) {
            return FlairChangeResult::INVALID_FLAIR_CLASS;
        }
        return FlairChangeResult::VALID;
    }

    public static function isValidFlairText($text) {
        if (strlen($text) > 64) {
            return FlairChangeResult::FLAIR_TEXT_TOO_LONG;
        }
        return FlairChangeResult::VALID;
    }

    public static function isValidAvailability($css_class) {
        if (is_admin()) {
            return FlairChangeResult::VALID;
        }
        if (!FlairConfig::is_class_available($css_class)) {
            return FlairChangeResult::NOT_AUTHORIZED;
        }
        return FlairChangeResult::VALID;
    }

    // set flair
    // --------------------------------------------------------------------------------
    public static function set_flair($user, $flair_class, $flair_text = '') {
        if (($valid_class = self::isValidFlairClass($flair_class)) !== FlairChangeResult::VALID) {
            return $valid_class;
        }

        if (($valid_text = self::isValidAvailability($flair_class)) !== FlairChangeResult::VALID) {
            return $valid_text;
        }

        if (($valid_text = self::isValidFlairText($flair_text)) !== FlairChangeResult::VALID) {
            return $valid_text;
        }

        $res = RedditMod::get_client()->fetch(
            self::get_endpoint('SET_FLAIR'),
            array(
                'name'      => $user,
                'css_class' => $flair_class,
                'text'      => $flair_text,
            ),
            \OAuth2Client\Client::HTTP_METHOD_POST
        );

        if (from($res['result'], 'success', 'to_bool')) {
            return FlairChangeResult::SUCCESS;
        } else {
            return FlairChangeResult::REDDIT_REJECTED;
        }
    }

}
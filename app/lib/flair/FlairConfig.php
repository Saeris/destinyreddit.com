<?php
namespace app\lib\flair;

use app\lib\flair\FlairManager;

class FlairConfig {
    const CONFIG = array(
        'sheet' => [
            'width' => 608,
            'height' => 608,
            'rows' => 8,
            'columns' => 8,
            'scale' => 0.6842105263157894736,
        ],
        'cell' => [
            'width' => 76,
            'height' => 76,
        ],
        'sheets' => [
            'SS1' => [
// FORMAT:      '{flair_unique_name}'       => [{display_name}, {availability}, {category}]
//              If {availability} is false, the flair is only available to administrators.
                '8bithunter'                => ['8-bit Hunter', true, '8-bit'],
                '8bittitan'                 => ['8-bit Titan', true, '8-bit'],
                '8bitwarlock'               => ['8-bit Warlock', true, '8-bit'],
                'HunterLogo'                => ['Hunter Logo', true, 'Class'],
                'Osiris'                    => ['Osiris', true, 'Misc'],
                'TitanLogo'                 => ['Titan Logo', true, 'Class'],
                'WarlockLogo'               => ['Warlock Logo', true, 'Class'],
                'SevenSeraphs'              => ['Seven Seraphs', true, ''],

                'bungieday05'               => ['Bungie Day 05', true, 'Class'],
                'bungieday06'               => ['Bungie Day 06', true, 'Class'],
                'bungieday07'               => ['Bungie Day 07', true, 'Class'],
                'bungieday08'               => ['Bungie Day 08', true, 'Class'],
                'bungieday09'               => ['Bungie Day 09', true, 'Class'],
                'bungieday10'               => ['Bungie Day 10', true, 'Class'],
                'bungieday11'               => ['Bungie Day 11', true, 'Class'],
                'bungieday01'               => ['Bungie Day 01', true, 'Misc'],

                'bungieday12'               => ['Bungie Day 12', true, 'Class'],
                'bungieday13'               => ['Bungie Day 13', true, 'Class'],
                'bungieday14'               => ['Bungie Day 14', true, 'Character'],
                'bungieday15'               => ['Bungie Day 15', true, 'Enemy'],
                'bungieday16'               => ['Bungie Day 16', true, 'Class'],
                'bungieday17'               => ['Bungie Day 17', true, 'Enemy'],
                'bungieday18'               => ['Bungie Day 18', true, 'Enemy'],
                'bungieday02'               => ['Bungie Day 02', true, 'Class'],

                'bungieday19'               => ['Bungie Day 19', true, 'Enemy'],
                'bungieday20'               => ['Bungie Day 20', true, 'Enemy'],
                'bungieday21'               => ['Bungie Day 21', true, 'Enemy'],
                'bungieday22'               => ['Bungie Day 22', true, 'Class'],
                'bungieday23'               => ['Bungie Day 23', true, 'Class'],
                'bungieday24'               => ['Bungie Day 24', true, 'Class'],
                'AgentoftheNine'            => ['Xur, Agent of the Nine', true, 'Character'],
                'bungieday04'               => ['Bungie Day 04', true, 'Class'],

                'BountyTracker'             => ['Xander 99-40, Bounty Tracker', true, 'Character'],
                'CrucibleHandler'           => ['Lord Shaxx, Crucible Handler', true, 'Character'],
                'CrucibleQuartermaster'     => ['Arcite 99-40, Crucible Quartermaster', true, 'Character'],
                'Cryptarch'                 => ['Master Rahool, Cryptarch', true, 'Character'],
                'DeadOrbitV'                => ['Arach Jalaal, Dead Orbit', true, 'Character'],
                'FutureWarCultV'            => ['Lakshmi-2, Future War Cult', true, 'Character'],
                'GuardianOutfitter'         => ['Eva Levante, Guardian Outfitter', true, 'Character'],
                'bungieday03'               => ['Bungie Day 03', true, 'Class'],

                'Gunsmith'                  => ['Banshee-44, Gunsmith', true, 'Character'],
                'HunterVanguard'            => ['Cayde-6, Hunter Vanguard', true, 'Character'],
                'NewMonarchy'               => ['Executor Hideo, New Monarchy', true, 'Character'],
                'Postmaster'                => ['Kadi 55-30, Postmaster', true, 'Character'],
                'QueensEmissary'            => ['Queen\'s Emissary', true, 'Character'],
                'Shipwright'                => ['Amanda Holiday, Shipwright', true, 'Character'],
                'SpecialOrders'             => ['Tess Everis, Special Orders', true, 'Character'],
                'Destiny04'                 => ['Tricorn', true, 'Misc'],

                'TheSpeaker'                => ['The Speaker', true, 'Character'],
                'TitanVanguard'             => ['Commander Zavala, Titan Vanguard', true, 'Character'],
                'VanguardQuartermaster'     => ['Roni 55-30, Vanguard Quartermaster', true, 'Character'],
                'WarlockVanguard'           => ['Ikora Rey, Warlock Vanguard', true, 'Character'],
                'Destiny01'                 => ['Thrall Art 1', true, 'Enemy'],
                'Destiny02'                 => ['Thrall Art 2', true, 'Enemy'],
                'Destiny03'                 => ['Taken Vandal Art', true, 'Enemy'],
                'Destiny05'                 => ['Exo Art', true, 'Race'],

                'Destiny06'                 => ['Hunter Art', true, 'Class'],
                'Destiny07'                 => ['Glimmer', true, 'Misc'],
                'Destiny08'                 => ['Titan Concept', true, 'Class'],
                'Destiny09'                 => ['Hunter Concept', true, 'Class'],
                'Destiny10'                 => ['Warlock Concept', true, 'Class'],
                'Destiny11'                 => ['Titan Art', true, 'Class'],
                'Destiny12'                 => ['Guardian on the Moon', true, 'Class'],
                'Destiny13'                 => ['Vandal Art', true, 'Enemy'],
            ],
            'SS2' => [
                'Destiny14'                 => ['Earthrise', true, 'Location'],
                'Destiny15'                 => ['Fallen Pike', true, 'Misc'],
                'Destiny16'                 => ['Traveler', true, 'Traveler'],
                'Destiny17'                 => ['Goblin Art', true, 'Enemy'],
                'Destiny18'                 => ['Hellmouth', true, 'Location'],
                'Destiny19'                 => ['Hunter Crest', true, 'Class'],
                'Destiny20'                 => ['Vandal Art 2', true, 'Enemy'],
                'Destiny21'                 => ['Bungie Crest', true, 'Misc'],

                'Destiny22'                 => ['Fallen Symbol', true, 'Enemy'],
                'Destiny23'                 => ['Traveler 2', true, 'Misc'],
                'Destiny24'                 => ['Destiny 24', true, 'Misc'],
                'Destiny26'                 => ['Tricorn', true, 'Misc'],
                'AbAeterno'                 => ['Ab Aeterno', true, 'Emblem'],
                'AbraxasII'                 => ['Abraxas II', true, 'Emblem'],
                'Abraxas'                   => ['Abraxas', true, 'Emblem'],
                'AlchemistCast'             => ['Alchemist Cast', true, 'Emblem'],

                'ArchivistsSeal'            => ['Archivist\'s Seal', true, 'Emblem'],
                'AspectofBlood'             => ['Aspects of Blood', true, 'Emblem'],
                'AspectofDust'              => ['Aspect of Dust', true, 'Emblem'],
                'AspectofShadow'            => ['Aspect of Shadow', true, 'Emblem'],
                'BadgeoftheMonarchyII'      => ['Badge of the Monarchy II', true, 'Emblem'],
                'BadgeoftheMonarchy'        => ['Badge of the Monarchy', true, 'Emblem'],
                'BadgeofthePatronII'        => ['Badge of the Patron II', true, 'Emblem'],
                'BadgeofthePatron'          => ['Badge of the Patron', true, 'Emblem'],

                'BindingFocus'              => ['Binding Focus', true, 'Emblem'],
                'BlessingofIV'              => ['Blessing of IV', true, 'Emblem'],
                'BlessingofWorlds'          => ['Blessing of Worlds', true, 'Emblem'],
                'BlessingoftheAncients'     => ['Blessing of the Ancients', true, 'Emblem'],
                'BlessingoftheGifted'       => ['Blessing of the Gifted', true, 'Emblem'],
                'BlessingoftheJoined'       => ['Blessing of the Joined', true, 'Emblem'],
                'BlessingoftheKnight'       => ['Blessing of the Knight', true, 'Emblem'],
                'BlessingoftheSentinel'     => ['Blessing of the Sentinel', true, 'Emblem'],

                'BlessingoftheSkeptic'      => ['Blessing of the Skeptic', true, 'Emblem'],
                'BlessingoftheSpeaker'      => ['Blessing of the Speaker', true, 'Emblem'],
                'BlessingoftheUnmade'       => ['Blessing of the Unmade', true, 'Emblem'],
                'BlessingoftheWatcher'      => ['Blessing of the Watcher', true, 'Emblem'],
                'BlessingoftheZealot'       => ['Blessing of the Zealot', true, 'Emblem'],
                'BombSquadII'               => ['Bomb Squad II', true, 'Emblem'],
                'BombSquad'                 => ['Bomb Squad', true, 'Emblem'],
                'BornofFire'                => ['Born of Fire', true, 'Emblem'],

                'Cassoid'                   => ['Cassoid', true, 'Emblem'],
                'CityForce'                 => ['City Force', true, 'Emblem'],
                'CommandII'                 => ['Command II', true, 'Emblem'],
                'Command'                   => ['Command', true, 'Emblem'],
                'CommanderCrest'            => ['Commander Crest', true, 'Emblem'],
                'CrestoftheGravesinger'     => ['Crest of the Gravesinger', true, 'Emblem'],
                'CrownoftheNewMonarchy'     => ['Crown of the New Monarchy', true, 'Emblem'],
                'CrownoftheSovereign'       => ['Crown of the Sovereign', true, 'Emblem'],

                'CruxLomar'                 => ['Crux Lomar', true, 'Emblem'],
                'CryptoShiftII'             => ['Crypto Shift II', true, 'Emblem'],
                'CryptoShift'               => ['Crypto Shift', true, 'Emblem'],
                'CyclopsMindII'             => ['Cyclops Mind II', true, 'Emblem'],
                'CyclopsMind'               => ['Cyclops Mind', true, 'Emblem'],
                'Daito'                     => ['Daito', true, 'Emblem'],
                'DeadZoneMemento'           => ['Dead Zone Memento', true, 'Emblem'],
                'EchoofShatteredSuns'       => ['Echo of Shattered Suns', true, 'Emblem'],

                'ElementoftheDeadSun'       => ['Element of the Dead Sun', true, 'Emblem'],
                'ElementoftheShifter'       => ['Element of the Shifter', true, 'Emblem'],
                'EmeraldRook'               => ['Emerald Rook', true, 'Emblem'],
                'ExecutorsRedMark'          => ['Executor\'s Red Mark', true, 'Emblem'],
                'FieldofLight'              => ['Field of Light', true, 'Emblem'],
                'FlamesofForgottenTruth'    => ['Flames of Forgotten Truth', true, 'Emblem'],
                'FoundersSeal'              => ['Founder\'s Seal', true, 'Emblem'],
                'FromHereTheStars'          => ['From Here, The Stars', true, 'Emblem'],
            ],
            'SS3' => [
                'GenGolgothaII'             => ['Gen Golgotha II', true, 'Emblem'],
                'GenGolgotha'               => ['Gen Golgotha', true, 'Emblem'],
                'HU00e4kke'                 => ['Hakke', true, 'Emblem'],
                'HoneybeeIV'                => ['Honeybee IV', true, 'Emblem'],
                'HonorofBlades'             => ['Honor of Blades', true, 'Emblem'],
                'Hunger'                    => ['Hunger', true, 'Emblem'],
                'IllusionofLight'           => ['Illusion of Light', true, 'Emblem'],
                'JadeRabbitInsignia'        => ['Jade Rabbit Insignia', true, 'Emblem'],

                'LoneFocusJaggedEdge'       => ['Lone Focus Jagged Edge', true, 'Emblem'],
                'MammothII'                 => ['Mammoth II', true, 'Emblem'],
                'Mammoth'                   => ['Mammoth', true, 'Emblem'],
                'MentorsBadge'              => ['Mentors Badge', true, 'Emblem'],
                'Nadir'                     => ['Nadir', true, 'Emblem'],
                'NoteofConquest'            => ['Note of Conquest', true, 'Emblem'],
                'OfficerCrest'              => ['Officer Crest', true, 'Emblem'],
                'OmenofChaosII'             => ['Omen of Chaos II', true, 'Emblem'],

                'OmenofChaos'               => ['Omen of Chaos', true, 'Emblem'],
                'OmenoftheDeadII'           => ['Omen of the Dead II', true, 'Emblem'],
                'OmenoftheDead'             => ['Omen of the Dead', true, 'Emblem'],
                'OmenoftheDecayer'          => ['Omen of the Decayer', true, 'Emblem'],
                'OmenoftheExodus'           => ['Omen of the Exodus', true, 'Emblem'],
                'Omolon'                    => ['Omolon', true, 'Emblem'],
                'PathfinderSign'            => ['Pathfinder Sign', true, 'Emblem'],
                'QueensGuardCrest'          => ['Queens Guard Crest', true, 'Emblem'],

                'Revolver'                  => ['Revolver', true, 'Emblem'],
                'RisingLight'               => ['Rising Light', true, 'Emblem'],
                'RuneoftheAdept'            => ['Rune of the Adept', true, 'Emblem'],
                'RuneoftheDisciple'         => ['Rune of the Disciple', true, 'Emblem'],
                'RuneoftheMachine'          => ['Rune of the Machine', true, 'Emblem'],
                'RuneoftheOracle'           => ['Rune of the Oracle', true, 'Emblem'],
                'ScarofRadegast'            => ['Scar of Radegast', true, 'Emblem'],
                'ScholarsQuest'             => ['Scholar\'s Quest', true, 'Emblem'],

                'ShelteredTruthII'          => ['Sheltered Truth II', true, 'Emblem'],
                false,
                'ShieldofHeroes'            => ['Shield of Heroes', true, 'Emblem'],
                'ShieldofLegends'           => ['Shield of Legends', true, 'Emblem'],
                'ShieldofMythics'           => ['Shield of Mythics', true, 'Emblem'],
                'ShieldoftheKnight'         => ['Shield of the Knight', true, 'Emblem'],
                'ShieldoftheWarlord'        => ['Shield of the Warlord', true, 'Emblem'],
                'SigilofDeviance'           => ['Sigil of Deviance', true, 'Emblem'],

                'SigilofSeven'              => ['Sigil of Seven', true, 'Emblem'],
                'SigiloftheBurningDawn'     => ['Sigil of the Burning Dawn', true, 'Emblem'],
                'SigiloftheComingWar'       => ['Sigil of the Coming War', true, 'Emblem'],
                'SigiloftheEternalNight'    => ['Sigil of the Eternal Night', true, 'Emblem'],
                'SigiloftheIronLords'       => ['Sigil of the Iron Lords', true, 'Emblem'],
                'SigiloftheWarCultII'       => ['Sigil of the War Cult II', true, 'Emblem'],
                false,
                'SignofContainment'         => ['Sign of Containment', true, 'Emblem'],

                'SignofDuality'             => ['Sign of Duality', true, 'Emblem'],
                false,
                'SignofUnity'               => ['Sign of Unity', true, 'Emblem'],
                'SignoftheAncients'         => ['Sign of the Ancients', true, 'Emblem'],
                'SignoftheBattleborn'       => ['Sign of the Battleborn', true, 'Emblem'],
                'SignoftheElders'           => ['Sign of the Elders', true, 'Emblem'],
                'SignoftheFinite'           => ['Sign of the Finite', true, 'Emblem'],
                'SignoftheFounders'         => ['Sign of the Founders', true, 'Emblem'],

                'SignoftheProtectorate'     => ['Sign of the Protectorate', true, 'Emblem'],
                'SongoftheSpheres'          => ['Song of the Spheres', true, 'Emblem'],
                'StarAntigen'               => ['Star Antigen', true, 'Emblem'],
                'StarofModeration'          => ['Star of Moderation', true, 'Emblem'],
                'Suros'                     => ['Suros', true, 'Emblem'],
                'SymboloftheMagister'       => ['Symbol of the Magister', true, 'Emblem'],
                'SymboloftheSorcerer'       => ['Symbol of the Sorcerer', true, 'Emblem'],
                'SymboloftheWolf'           => ['Symbol of the Wolf', true, 'Emblem'],
            ],
            'SS4' => [
                'TexMechanica'              => ['Tex Mechanica', true, 'Emblem'],
                'TheConvergence'            => ['The Convergence', true, 'Emblem'],
                'TheInnerChamber'           => ['The Inner Chamber', true, 'Emblem'],
                'TheObeliskII'              => ['The Obelisk II', true, 'Emblem'],
                'TheObelisk'                => ['The Obelisk', true, 'Emblem'],
                'TheReflectiveProof'        => ['The Reflective Proof', true, 'Emblem'],
                'TheRisingNight'            => ['The Rising Night', true, 'Emblem'],
                'TheRising'                 => ['The Rising', true, 'Emblem'],

                'TheUndyingLight'           => ['The Undying Light', true, 'Emblem'],
                'TheUnimaginedPlane'        => ['The Unimagined Plane', true, 'Emblem'],
                'TheWindingPath'            => ['The Winding Path', true, 'Emblem'],
                'ThoriumLeaf'               => ['Thorium Leaf', true, 'Emblem'],
                'TipoftheSpear'             => ['Tip of the Spear', true, 'Emblem'],
                'Transcendence'             => ['Transcendence', true, 'Emblem'],
                'UnionofLight'              => ['Union of Light', true, 'Emblem'],
                'VanguardHonor'             => ['Vanguard Honor', true, 'Emblem'],

                'VanguardInsignia'          => ['Vanguard Insignia', true, 'Emblem'],
                'VeteranCrest'              => ['Veteran Crest', true, 'Emblem'],
                'VictoryEagleII'            => ['Victory Eagle II', true, 'Emblem'],
                'WinterbornMark'            => ['Winterborn Mark', true, 'Emblem'],
                'WorldDomination'           => ['World Domination', true, 'Emblem'],
                'osiris'                    => ['Osiris', true, 'Emblem'],
                'sq'                        => ['Squeagley Special Flair', false, 'Admin'],
                'Banner1'                   => ['Banner 1', true, 'Medal'],

                'seventhcolumn'             => ['7th Seventh Column', true, 'Medal'],
                'Banner3'                   => ['Banner 3', true, 'Medal'],
                'Banner4'                   => ['Banner 4', true, 'Medal'],
                'Banner5'                   => ['Banner 5', true, 'Medal'],
                'CarnageZone'               => ['Carnage Zone', true, 'Misc'],
                'SpaceSquirrel'             => ['Space Squirrel', true, 'Misc'],
                'firenfeathers'             => ['Firen feathers', true, 'Misc'],
                'seventhcolumn7'            => ['Seventh column 7', true, 'Medal'],

                'seventhcolumnred'          => ['7th Seventh Column Red', true, 'Medal'],
                'shield1'                   => ['Shield 1', true, 'Misc'],
                'shield2'                   => ['Shield 2', true, 'Misc'],
                'soffish'                   => ['Soffish', true, 'Misc'],
                'tigerman'                  => ['Tigerman', true, 'Misc'],
                'userresearch'              => ['User research', true, 'Misc'],
                'EagleMatt'                 => ['Matty McD\'s Special Flair', false, 'Admin'],
                'BlackTiger'                => ['Black Tiger', true, ''],

                'ninja'                     => ['Bungie.net Forum Ninja Special Flair', false, 'Admin'],
                'dcflair'                   => ['DemonCipher13 Special Flair', false, 'Admin'],
                'concordat'                 => ['Concordat', true, 'Misc'],
                'doug'                      => ['doug/tug Special Flair', false, 'Admin'],
                '77AdInfinitum'             => ['77 Ad Infinitum', true, 'Emblem'],
                'AnchorsEnd'                => ['Anchors End', true, ''],
                'ArchersHope'               => ['Archers Hope', true, ''],
                'BlessingoftheUnmadeAlt'    => ['Blessing of the Unmade Alt', true, 'Emblem'],

                'BombSquadAlt'              => ['Bomb Squad Alt', true, 'Emblem'],
                'BombSquadIIAlt'            => ['Bomb Squad II Alt', true, 'Emblem'],
                'CassoidAlt'                => ['Cassoid Alt', true, 'Emblem'],
                'DawnofDestiny'             => ['Dawn of Destiny', true, 'Emblem'],
                'Earthborn'                 => ['Earthborn', true, 'Emblem'],
                'EchoofShatteredSunsAlt'    => ['Echo of Shattered Suns Alt', true, 'Emblem'],
                'EmperorSigil'              => ['Emperor Sigil', true, 'Emblem'],
                'EyeofEternity'             => ['Eye of Eternity', true, 'Emblem'],

                'EyeofOsiris'               => ['Eye of Osiris', true, 'Emblem'],
                'GenGolgothaAlt'            => ['Gen Golgotha Alt', true, 'Emblem'],
                'GenGolgothaIIAlt'          => ['Gen Golgotha II Alt', true, 'Emblem'],
                'HeartoftheFoundation'      => ['Heart of the Foundation', true, 'Emblem'],
                'Hexacon4'                  => ['Hexacon 4', true, ''],
                'MoonofOsiris'              => ['Moon of Osiris', true, 'Emblem'],
                'PrinceSigil'               => ['Prince Sigil', true, 'Emblem'],
                'Resurrectionist'           => ['Resurrectionist', true, 'Emblem'],
            ],
            'SS5' => [
                'RuneoftheMachineAlt'       => ['Rune of the Machine Alt', true, 'Emblem'],
                'ShelteredTruthAlt'         => ['Sheltered Truth Alt', true, 'Emblem'],
                'ShelteredTruthIIAlt'       => ['Sheltered Truth II Alt', true, 'Emblem'],
                'SigilofSevenAlt'           => ['Sigil of Seven Alt', true, 'Emblem'],
                'SunofOsiris'               => ['Sun of Osiris', true, 'Emblem'],
                'Suros'                     => ['Suros', true, 'Emblem'],
                'TheInnerCircle'            => ['The Inner Circle', true, 'Emblem'],
                'VanguardHonorAlt'          => ['Vanguard Honor Alt', true, 'Emblem'],

                'SigilOfNight'              => ['Sigil Of Night', true, 'Emblem'],
                'DarkHarvest'               => ['Dark Harvest', true, 'Emblem'],
                'BladeOfCrota'              => ['Blade Of Crota', true, 'Emblem'],
                'CrotasEnd'                 => ['Crotas End', true, 'Emblem'],
                'ErisMorn'                  => ['Crota\'s Bane', true, 'Emblem'],
                'Eris'                      => ['Eris Morn, Crota\'s Bane', true, 'Character'],
                'ExoStranger'               => ['Exo Stranger', true, 'Character'],
                'LordSaladin'               => ['Lord Saladin', true, 'Character'],

                'Riksis'                    => ['Riksis, Devil Archon', true, 'Enemy'],
                'Simiks'                    => ['Simiks-3', true, 'Enemy'],
                'Draksis'                   => ['Draksis, Winter Kell', true, 'Enemy'],
                'Septiks'                   => ['Sepiks Prime', true, 'Enemy'],
                'Aksor'                     => ['Aksor, Archon Priest', true, 'Enemy'],
                'Kranox'                    => ['Kranox, the Graven', true, 'Enemy'],
                'SwarmPrince'               => ['Swarm Prince', true, 'Enemy'],
                'Telthor'                   => ['Telthor, Unborn', true, 'Enemy'],

                'Sardok'                    => ['Sardok, Eye of Oryx', true, 'Enemy'],
                'Mormu'                     => ['Mormu, Xol Spawn', true, 'Enemy'],
                'Phogoth'                   => ['Phogoth, the Untamed', true, 'Enemy'],
                'Sardon'                    => ['Sardon, Fist of Crota', true, 'Enemy'],
                'CrotasMight'               => ['Might of Crota', true, 'Enemy'],
                'CrotasHand'                => ['Hand of Crota', true, 'Enemy'],
                'CrotasEye'                 => ['Eyes of Crota', true, 'Enemy'],
                'CrotasHeart'               => ['Heart of Crota', true, 'Enemy'],

                'Urzok'                     => ['Urzok, the Hated', true, 'Enemy'],
                'Forsaken'                  => ['The Forsaken', true, 'Enemy'],
                'Omnigal'                   => ['Omnigul, Will of Crota', true, 'Enemy'],
                'IrYut'                     => ['Ir Y�t, Deathsinger', true, 'Enemy'],
                'Crota'                     => ['Crota, Son of Oryx', true, 'Enemy'],
                'Zydron'                    => ['Zydron, Gatelord', true, 'Enemy'],
                'Prohibitive'               => ['Prohibitive Mind', true, 'Enemy'],
                'SolProgeny'                => ['Sol Progeny', true, 'Enemy'],

                'Sekrion'                   => ['Sekrion, Nexus Mind', true, 'Enemy'],
                'Gorgon'                    => ['Gorgon', true, 'Enemy'],
                'VexTemplar'                => ['The Templar', true, 'Enemy'],
                'UndyingMind'               => ['The Undying Mind', true, 'Enemy'],
                'Atheon'                    => ['Atheon, Time\'s Conflux', true, 'Enemy'],
                'Thoourg'                   => ['Bracus Tho\'ourg', true, 'Enemy'],
                'Thaaurn'                   => ['Bracus Tha\'aurn', true, 'Enemy'],
                'Shaaull'                   => ['Primus Sha\'aull', true, 'Enemy'],

                'Psionflayers'              => ['Psion flayers', true, 'Enemy'],
                'Taaurc'                    => ['Valus Ta\'aurc', true, 'Enemy'],
                'Dragoon'                   => ['Dragoon', true, 'Emblem'],
                'WolfsGrin'                 => ['Wolfs Grin', true, 'Emblem'],
                'FallenLogo'                => ['Fallen Logo', true, 'Enemy Race'],
                'HiveLogo'                  => ['Hive Logo', true, 'Enemy Race'],
                'VexLogo'                   => ['Vex Logo', true, 'Enemy Race'],
                'CabalLogo'                 => ['Cabal Logo', true, 'Enemy Race'],

                'Crucible'                  => ['Crucible', true, 'Activity'],
                'Story'                     => ['Story', true, 'Activity'],
                'Strike'                    => ['Strike', true, 'Activity'],
                'Void'                      => ['Void', true, 'Element'],
                'Solar'                     => ['Solar', true, 'Element'],
                'Arc'                       => ['Arc', true, 'Element'],
                'Queen'                     => ['Queen Mara Sov', true, 'Character'],
                'QueensBro'                 => ['Uldren Sov', true, 'Character'],
            ],
            'SS6' => [
                'E347'                      => ['347 Vesta Dynasty', true, 'Weapon'],
                'EBJ'                       => ['Bad JuJu', true, 'Weapon'],
                'EFAOF'                     => ['Fate of All Fools', true, 'Weapon'],
                'EHL'                       => ['Hard Light', true, 'Weapon'],
                'EHM'                       => ['Hawkmoon', true, 'Weapon'],
                'EMC'                       => ['Monte Carlo', true, 'Weapon'],
                'EMIDA'                     => ['MIDA Multitool', true, 'Weapon'],
                'ENC'                       => ['Necrochasm', true, 'Weapon'],

                'ENLB'                      => ['No Land Beyond', true, 'Weapon'],
                'ERD'                       => ['Red Death', true, 'Weapon'],
                'ESR'                       => ['Suros Regime', true, 'Weapon'],
                'ET'                        => ['Thorn', true, 'Weapon'],
                'ETLW'                      => ['The Last Word', true, 'Weapon'],
                'EUR'                       => ['Universal Remote', true, 'Weapon'],
                'EVM'                       => ['Vex Mythoclast', true, 'Weapon'],
                'EIB'                       => ['Ice Breaker', true, 'Weapon'],

                'EINV'                      => ['Invective', true, 'Weapon'],
                'ELOW'                      => ['Lord of Wolves', true, 'Weapon'],
                'EPAT'                      => ['Patience and Time', true, 'Weapon'],
                'EPI'                       => ['Pocket Infinity', true, 'Weapon'],
                'EPLANC'                    => ['Plan C', true, 'Weapon'],
                'ET4TH'                     => ['4th Horseman', true, 'Weapon'],
                'EDB'                       => ['Dragon\'s Breath', true, 'Weapon'],
                'EGH'                       => ['Gjallarhorn', true, 'Weapon'],

                'ESGA'                      => ['Super Good Advice', true, 'Weapon'],
                'ETL'                       => ['Thunderlord', true, 'Weapon'],
                'ETRUTH'                    => ['Truth', true, 'Weapon'],
                'CrotaCheese'               => ['Crota Cheese Hat', true, 'Misc'],
                'sleeper'                   => ['Sleeper', true, 'Weapon'],
                'ES99'                      => ['S-99 Dawnchaser', true, 'Sparrow'],
                'array'                     => ['No Puppet, I', true, 'Emblem'],
                'SpookyPumpkin'             => ['Spooky Pumpkin', true, 'Misc'],

                'HolidayCryptarch'          => ['Holiday Cryptarch', true, 'Character'],
                'BeachDreg'                 => ['Beach Dreg', true, 'Misc'],
                'WarlockSunsinger'          => ['Warlock Sunsinger', true, 'Class'],
                'starwolf'                  => ['Starwolf', true, 'Emblem'],
                'cryptarch'                 => ['Cryptarch Logo', true, 'Character'],
                'purpleboom'                => ['Purple Boom', true, 'Emblem'],
                'regal'                     => ['Regal DSS', false, 'Contest'],
                'CalusCheese'               => ['Calus Cheese', true, 'Misc'],

                'engram'                    => ['Engram', true, 'Emblem'],
                'rose'                      => ['Rose', true, 'Emblem'],
                'bladedancer'               => ['Bladedancer', true, 'Class'],
                'stormcaller'               => ['Stormcaller', true, 'Class'],
                'striker'                   => ['Striker', true, 'Class'],
                'sunsinger'                 => ['Sunsinger', true, 'Class'],
                'sunbreaker'                => ['Sunbreaker', true, 'Class'],
                'gunslinger'                => ['Gunslinger', true, 'Class'],

                'defender'                  => ['Defender', true, 'Class'],
                'voidwalker'                => ['Voidwalker', true, 'Class'],
                'voidstalker'               => ['Nightstalker', true, 'Class'],
                'GI'                        => ['Gameinformer Employee', false, 'Admin'],
                'sherpa'                    => ['Reddit Sherpa', false, 'Admin'], // r/TheMountainTop for more info. Represents a r/DestinySherpa or r/CrucibleSherpa, sherpa.
                'vvisions'                  => ['Vicarious Visions Employee', false, 'Admin'], // They are an employee of Vicarious Visions, the company helping to developer Destiny 2 on PC
                'activision'                => ['Activision Employee', false, 'Admin'],
                'clarkey'                   => ['Clarkey7163\'s Special Flair', false, 'Admin'],

                'GrimoireWriter'            => ['Bungie Grimoire Writer', false, 'Admin'],
                'Woodhouse'                 => ['Woodhouse Special Flair', false, 'Admin'],
                'limacat'                   => ['limacat', false, 'Admin'],
                'salt'                      => ['Salt', true, 'Misc'], // DTG Salt Flair
                'Snoo1'                     => ['DTG Snoo', false, 'Admin'],
                'Verified-Bungie-Employee'  => ['Verified Bungie Employee', false, 'Admin'], // These are both the same image, but have 2 diff CSS class names
                'skynet'                    => ['SkyNet Logo', false, 'Admin'],
                'mgs'                       => ['MetalGilSolid Special Flair', false, 'Admin'],
            ],
            'SS7' => [
                'NorseWolf'                 => ['Norse Wolf Special Flair', false, 'Admin'],
                'Fuzzle'                    => ['Fuzzle Special Flair', false, 'Admin'],
                'ZaraFWC'                   => ['Zara FWC Special Flair', false, 'Admin'],
                'TigermanGif'               => ['Tigerman GIF', false, 'Former GIF'],
                'Tahryl'                    => ['Tahryl Special Flair', false, 'Admin'],
                'OxInBox'                   => ['Ox\'s Special Flair', false, 'Admin'],
                'OgreRaisins'               => ['Ogre Raisins', true, 'Misc'],
                '3oCLego'                   => ['GreenLego 3 of Coins', false, 'Admin'],

                'NorseBdayWolf'             => ['Norse Bday Wolf Special Flair', false, 'Admin'],
                'SgtWarChicken'             => ['Sgt War Chicken', false, 'Admin'],
                'DinklebotGif'              => ['Ghost (non)GIF Special Flair', false, 'Former GIF'],
                'MSPaintSekrion'            => ['MS Paint Sekrion (non)GIF Special Flair', true, 'Former GIF'],
                'MSPaintOryx'               => ['MS Paint Oryx (non)GIF Special Flair', true, 'Former GIF'],
                'SnackDad'                  => ['Snack Dad Special Flair', false, 'Admin'], // Initially for /u/Soundurr | Ref: https://redd.it/7154a9
                'BrotherVance'              => ['Brother Vance', true, 'Character'],
                'EververseGiving'           => ['Eververse Giving', true, 'Misc'],

                'UgandaKnucklesVance'       => ['Uganda Knuckles Vance', false, 'Misc'],
                'DIM'                       => ['Destiny Item Manager (DIM)', false, 'Developer'],
                'IshtarCommander'           => ['Ishtar Commander', false, 'Developer'],
                'TRN'                       => ['Tracker Network (DTR)', false, 'Developer'],
                'TrialsReport'              => ['Trials.Report', false, 'Developer'],
                'GuardianTheater'           => ['Guardian Theater', false, 'Developer'],
                'VendorEngramsXYZ'          => ['VendorEngrams.xyz', false, 'Developer'],
                'DestinyVendorGearTracker'  => ['destiny-vendor-gear-tracker.com', false, 'Developer'],

                'LightGG'                   => ['Light.GG', false, 'Developer'],
                'DestinyKD'                 => ['DestinyKD', false, 'Developer'],
                'WastedOnDestiny'           => ['Wasted on Destiny', false, 'Developer'],
                'D2Checklist'               => ['D2Checklist.com', false, 'Developer'],
                'GuardianGG'                => ['Guardian.gg', false, 'Developer'],
                'Charlemagne'               => ['Charlemagne', false, 'Developer'],
                'IshtarCollective'          => ['Ishtar Collective', false, 'Developer'],
                'The100io'                  => ['The100.io', false, 'Developer'],

                'TheVaultItemManager'       => ['The Vault: Item Manager', false, 'Developer'],
                'DestinyClanWarfare'        => ['DestinyClanWarfare.com', false, 'Developer'],
                'DestinySets'               => ['DestinySets.com', false, 'Developer'],
                false,
                false,
                false,
                false,
                false,

                'AscendantRaisins'          => ['Ascendant Raisins', true, 'Misc'],
                'ValorInDarkness'           => ['Valor In Darkness', true, 'Emblem'],
                'GuardianLord'              => ['Guardian Lord', true, 'Emblem'],
                'SweeperBotPin'             => ['Sweeper Bot Pin', true, 'Misc'],
                'GatherYourFireteam'        => ['Gather Your Fireteam', true, 'Emblem'],
                'BabyGhaul'                 => ['Baby Ghaul', true, 'Misc'],
                'OwlSector'                 => ['Owl Sector', true, 'Emblem'],
                'CalusRobot'                => ['Calus Robot', true, 'Enemy'],

                'SagiraGhost'               => ['Sagira Ghost Shell', true, 'Ghost'],
                'Saint14Emblem'             => ['Saint-14 Emblem', true, 'Emblem'],
                'Saint14Ship'               => ['Saint-14 Gray Pigeon Ship', true, 'Ship'],
                'D2Y1FlawlessEmblem'        => ['Know Your Way Trials Flawless Emblem', true, 'Emblem'],
                'PrideOfNepal'              => ['Pride Of Nepal Emblem', true, 'Emblem'],
                'VeistLogo'                 => ['Veist Logo', true, 'Logo'],
                'CharredCelery'             => ['Charred Celery', true, 'Misc'],
                'AgeOfTriumph'              => ['Age Of Triumph', true, 'Misc'],

                'DMGEmote'                  => ['DMG Emote', true, 'Misc'],
                '2TAAB'                     => ['2 Tokens And A Blue', false, 'Misc'],
                'Zavala'                    => ['Zavala (Smiling)', true, 'Character'],
                'Failsafe'                  => ['Failsafe', true, 'Character'],
                'S32VSparrow'               => ['Sparrow S-32V', true, 'Sparrow'],
                'HawkmoonaMatata'           => ['HawkmoonaMatata Special Flair', false, 'Admin'],
                'Ruley9'                    => ['Ruley9 Special Flair', false, 'Admin'],
                false,

            ],
            'SS8' => [
                'CosmosShell'               => ['Cosmos Shell', true, 'Ghost'],
                'ElectronicaShell'          => ['Electronica Shell', true, 'Ghost'],
                'FastLaneShell'             => ['Fast Lane Shell', true, 'Ghost'],
                'FireVictoriousShell'       => ['Fire Victorious Shell', true, 'Ghost'],
                'StarMapShell'              => ['Star Map Shell', true, 'Ghost'],
                'WinterLotusShell'          => ['Winter Lotus Shell', true, 'Ghost'],
                'FlipOutEmote'              => ['Flip Out Emote', true, 'Emote'],
                'GivingEmote'               => ['Giving Emote', true, 'Emote'],

                'MicDropEmote'              => ['Mic Drop Emote', true, 'Emote'],
                'SaltyEmote'                => ['Salty Emote', true, 'Emote'],
                'SelfieEmote'               => ['Selfie Emote', true, 'Emote'],
                'SixShooterEmote'           => ['Six Shooter Emote', true, 'Emote'],
                'SpicyRamenEmote'           => ['Spicy Ramen Emote', true, 'Emote'],
                'SweepingEmote'             => ['Sweeping Emote', true, 'Emote'],
                'ChillofWinterSparrow'      => ['Chill of Winter Sparrow', true, 'Sparrow'],
                'ConcentricDawnSparrow'     => ['Concentric Dawn Sparrow', true, 'Sparrow'],

                'CurseofForesightSparrow'   => ['Curse of Foresight Sparrow', true, 'Sparrow'],
                'DinasEmrysSparrow'         => ['Dinas Emrys Sparrow', true, 'Sparrow'],
                'HastiludeSparrow'          => ['Hastilude Sparrow', true, 'Sparrow'],
                'HolidayCheerSparrow'       => ['Holiday Cheer Sparrow', true, 'Sparrow'],
                'SV-112PredatorSparrow'     => ['SV-112 Predator Sparrow', true, 'Sparrow'],
                'VanishingPointSparrow'     => ['Vanishing Point Sparrow', true, 'Sparrow'],
                'AHistoryofStarlightShip'   => ['A History of Starlight Ship', true, 'Ship'],
                'AsherMirShip'              => ['Asher Mir\'s One-Way Ticket Ship', true, 'Ship'],

                'CeruleanFlashShip'         => ['Cerulean Flash Ship', true, 'Ship'],
                'EgoandSquidShip'           => ['Ego and Squid Ship', true, 'Ship'],
                'ErianaVengeanceShip'       => ['Eriana\'s Vengeance Ship', true, 'Ship'],
                'IkoraResolveShip'          => ['Ikora\'s Resolve Ship', true, 'Ship'],
                'KabrGlassAegisShip'        => ['Kabr\'s Glass Aegis Ship', true, 'Ship'],
                'RoseandBoneShip'           => ['Rose and Bone Ship', true, 'Ship'],
                'SailsofOsirisShip'         => ['Sails of Osiris Ship', true, 'Ship'],
                'SymmetryFlightShip'        => ['Symmetry Flight Ship', true, 'Ship'],

                'TakanomeWingsShip'         => ['Takanome Wings Ship', true, 'Ship'],
                'TheBandwagonShip'          => ['The Bandwagon Ship', true, 'Ship'],
                'TheSundareshExp13-RShip'   => ['The Sundaresh Experiment 13-R Ship', true, 'Ship'],
                'BorealisWPN'               => ['Borealis Weapon', true, 'Weapon'],
                'ColdheartWPN'              => ['Coldheart Weapon', true, 'Weapon'],
                'CrimsonWPN'                => ['Crimson Weapon', true, 'Weapon'],
                'DARCIWPN'                  => ['DARCI Weapon', true, 'Weapon'],
                'FightingLionWPN'           => ['Fighting Lion Weapon', true, 'Weapon'],

                'GravitonLanceWPN'          => ['Graviton Lance Weapon', true, 'Weapon'],
                'HardLightWPN'              => ['Hard Light Weapon', true, 'Weapon'],
                'LegendofAcriusWPN'         => ['Legend of Acrius Weapon', true, 'Weapon'],
                'MercilessWPN'              => ['Merciless Weapon', true, 'Weapon'],
                'D2MidaWPN'                 => ['D2 MIDA Multi-Tool Weapon', true, 'Weapon'],
                'PrometheusLenseWPN'        => ['Prometheus Lense Weapon', true, 'Weapon'],
                'RatKingWPN'                => ['Rat King Weapon', true, 'Weapon'],
                'RiskrunnerWPN'             => ['Riskrunner Weapon', true, 'Weapon'],

                'SkyburnersOathWPN'         => ['Skyburners Oath Weapon', true, 'Weapon'],
                'SturmWPN'                  => ['Sturm Weapon', true, 'Weapon'],
                'SunshotWPN'                => ['Sunshot Weapon', true, 'Weapon'],
                'SweetBusinessWPN'          => ['Sweet Business Weapon', true, 'Weapon'],
                'D2TelestoWPN'              => ['D2 Telesto Weapon', true, 'Weapon'],
                'TheColonyWPN'              => ['The Colony Weapon', true, 'Weapon'],
                'D2JadeRabbitWPN'           => ['D2 Jade Rabbit Weapon', true, 'Weapon'],
                'TheProspectorWPN'          => ['The Prospector Weapon', true, 'Weapon'],

                'TheWardcliffCoilWPN'       => ['The Wardcliff Coil Weapon', true, 'Weapon'],
                'TractorCannonWPN'          => ['Tractor Cannon Weapon', true, 'Weapon'],
                'VigilanceWingWPN'          => ['Vigilance Wing Weapon', true, 'Weapon'],
                false,
                false,
                false,
                false,
                false,
            ],
            'SS9' => [
                'AndalBrask'                => ['Andal Brask', true, 'Character'],
                'CabalLegio'                => ['Cabal Legionary Art', true, 'Enemy'],
                'Emissary9'                 => ['Emissary of the Nine', true, 'Character'],
                'ErisCreepy'                => ['Eris Creepy Concept Art', true, 'Character'],
                'WizardArt'                 => ['Wizard Concept Art', true, 'Enemy'],
                'Human'                     => ['Human Art', true, 'Race'],
                'IkoraHunter'               => ['Ikora & Hunter', true, 'Character'],
                'JourneyTitan'              => ['City Exodus Art', true, 'Character'],

                'LadyJolderArt'             => ['Lady Jolder Art', true, 'Character'],
                'QueenArt'                  => ['Mara Sov Art', true, 'Character'],
                'OldSpeaker'                => ['Speaker Cinematic Concept Art', true, 'Character'],
                'OldShaxx'                  => ['Shaxx Cinematic Concept Art', true, 'Character'],
                'OldSaladin'                => ['Saladin Cinematic Concept Art', true, 'Character'],
                'TechWitch'                 => ['Techun Witch Art', true, 'Enemy'],
                'TakenDreg'                 => ['Taken Dreg Art', true, 'Enemy'],
                'TakenColossus'             => ['Ttaken Colossus Art', true, 'Enemy'],

                'LordSaladinArt'            => ['Saladin Art', true, 'Character'],
                'RiksisArt'                 => ['Riksis Art', true, 'Enemy'],
                'Saint-14Art'               => ['Saint-14\'s Tomb', true, 'Character'],
                'Gatelord'                  => ['Gatelord Zydron', true, 'Enemy'],
                'TravelerAwake'             => ['Traveler Awake', true, 'Traveler'],
                'TravelerCaged1'            => ['Traveler Caged 1', true, 'Traveler'],
                'TravelerCaged2'            => ['Traveler Caged 2', true, 'Traveler'],
                'HunterPoster'              => ['Hunter Poster Art', true, 'Class'],

                'Wolf1'                     => ['Iron Temple Wolf 1', true, 'Character'], 
                'Wolf2'                     => ['Iron Temple Wolf 2', true, 'Character'],
                'HammerofSol'               => ['Hammer of Sol Art', true, 'Class'],
                'DuskBow'                   => ['Dusk Bow Art', true, 'Class'],
                'Stormtrance'               => ['Stormtrance Art', true, 'Class'],
                'TitanSymbol'               => ['Titan Symbol Art', true, 'Class'],
                'HunterSymbol'              => ['Hunter Symbol Art', true, 'Class'],
                'WarlockSymbol'             => ['Warlock Symbol Art', true, 'Class'],

                'Sunbreaker1'               => ['Sunbreaker Art 1', true, 'Class'],
                'Sunbreaker2'               => ['Sunbreaker Art 2', true, 'Class'],
                'Nightstalker1'             => ['Nightstalker Art 1', true, 'Class'],
                'Nightstalker2'             => ['Nightstalker Art 2', true, 'Class'],
                'Stormcaller1'              => ['Stormcaller Art 1', true, 'Class'],
                'Stormcaller2'              => ['Stormcaller Art 2', true, 'Class'],
                'SunbreakerPromo'           => ['Sunbreaker Promo', true, 'Class'],
                'NightstalkerPromo'         => ['Nightstalker Promo', true, 'Class'],

                'StormcallerPromo'          => ['Stormcaller Promo', true, 'Class'],
                'D2Sunbreaker'              => ['Sunbreaker Subclass', true, 'Class'],
                'D2Striker'                 => ['Striker Subclass', true, 'Class'],
                'D2Sentinel'                => ['Sentinel Subclass', true, 'Class'],
                'D2Gunslinger'              => ['Gunslinger Subclass', true, 'Class'],
                'D2Arcstrider'              => ['Arcstrider Subclass', true, 'Class'],
                'D2Nightstalker'            => ['Nightstalker Subclass', true, 'Class'],
                'D2Dawnblade'               => ['Dawnblade Subclass', true, 'Class'],

                'D2Stormcaller'             => ['Stormcaller Subclass', true, 'Class'],
                'D2Voidwalker'              => ['Voidwalker Subclass', true, 'Class'],
                'MemoryDestiny'             => ['Memory Destiny', true, 'Misc'],
                'MemoryVoG'                 => ['Memory VoG', true, 'Misc'],
                'MemoryTDB'                 => ['Memory TDB', true, 'Misc'],
                'MemoryCE'                  => ['Memory CE', true, 'Misc'],
                'MemoryHoW'                 => ['Memory HoW', true, 'Misc'],
                'MemoryPoE'                 => ['Memory PoE', true, 'Misc'],

                'MemoryToO'                 => ['Memory ToO', true, 'Misc'],
                'MemoryTTK'                 => ['Memory TTK', true, 'Misc'],
                'MemoryKF'                  => ['Memory KF', true, 'Misc'],
                'MemoryRoI'                 => ['Memory RoI', true, 'Misc'],
                'MemoryWotM'                => ['Memory WotM', true, 'Misc'],
                'MemoryAoT'                 => ['Memory AoT', true, 'Misc'],
                'SigurnVictor'              => ['Sigurn & Victor Medallion', true, 'Misc'],
                'DestinyWarlock'            => ['Warlock Art', true, 'Class'],
            ],
            'SS10' => [
                'CHunter1'                  => ['Hunter Concept 1', true, 'Class'],
                'CHunter2'                  => ['Hunter Concept 2', true, 'Class'],
                'CHunter3'                  => ['Hunter Concept 3', true, 'Class'],
                'CTitan1'                   => ['Titan Concept 1', true, 'Class'],
                'CTitan2'                   => ['Titan Concept 2', true, 'Class'],
                'CTitan3'                   => ['Titan Concept 3', true, 'Class'],
                'CWarlock1'                 => ['Warlock Concept 1', true, 'Class'],
                'CWarlock2'                 => ['Warlock Concept 2', true, 'Class'],

                'CWarlock3'                 => ['Warlock Concept 3', true, 'Class'],
                'VoGTitan'                  => ['Vault of Glass Titan', true, 'Class'],
                'VoGHunter'                 => ['Vault of Glass Hunter', true, 'Class'],
                'VoGWarlock'                => ['Vault of Glass Warlock', true, 'Class'],
                'TrialsTitan1'              => ['Osiris Titan 1', true, 'Class'],
                'TrialsHunter1'             => ['Osiris Hunter 1', true, 'Class'],
                'TrialsWarlock1'            => ['Osiris Warlock 1', true, 'Class'],
                'TrialsTitan2'              => ['Osiris Titan 2', true, 'Class'],

                'TrialsHunter2'             => ['Osiris Hunter 2', true, 'Class'],
                'TrialsWarlock2'            => ['Osiris Warlock 3', true, 'Class'],
                'GensymTitan'               => ['Gensym Titan', true, 'Class'],
                'GensymHunter'              => ['Gensym Hunter', true, 'Class'],
                'GensymWarlock'             => ['Gensym Warlock', true, 'Class'],
                'CETitan'                   => ['Crota\'s End Titan', true, 'Class'],
                'CEHunter'                  => ['Crota\'s End Hunter', true, 'Class'],
                'CEWarlock'                 => ['Crota\'s End Warlock', true, 'Class'],

                'ATitan1'                   => ['Titan Armor 1', true, 'Class'],
                'ATitan2'                   => ['Titan Armor 2', true, 'Class'],
                'ATitan3'                   => ['Titan Armor 3', true, 'Class'],
                'ATitan4'                   => ['Titan Armor 4', true, 'Class'],
                'ATitan5'                   => ['Titan Armor 5', true, 'Class'],
                'ATitan6'                   => ['Titan Armor 6', true, 'Class'],
                'AHunter1'                  => ['Hunter Armor 1', true, 'Class'],
                'AHunter2'                  => ['Hunter Armor 2', true, 'Class'],

                'AHunter3'                  => ['Hunter Armor 3', true, 'Class'],
                'AHunter4'                  => ['Hunter Armor 4', true, 'Class'],
                'AHunter5'                  => ['Hunter Armor 5', true, 'Class'],
                'AHunter6'                  => ['Hunter Armor 6', true, 'Class'],
                'AWarlock1'                 => ['Warlock Armor 1', true, 'Class'],
                'AWarlock2'                 => ['Warlock Armor 2', true, 'Class'],
                'AWarlock3'                 => ['Warlock Armor 3', true, 'Class'],
                'AWarlock4'                 => ['Warlock Armor 4', true, 'Class'],

                'AWarlock5'                 => ['Warlock Armor 5', true, 'Class'],
                'NessusWarlock'             => ['Warlock on Nessus', true, 'Class'],
                'IoWarlock'                 => ['Warlock on Io', true, 'Class'],
                'TitanBlack'                => ['Titan in Black', true, 'Class'],
                'TitanRed'                  => ['Titan in Red', true, 'Class'],
                'TitanGold'                 => ['Titan in Gold', true, 'Class'],
                'TitanBlue'                 => ['Titan in Blue', true, 'Class'],
                'TitanGray'                 => ['Titan in Gray', true, 'Class'],

                'TitanSUROS'                => ['Titan in SUROS', true, 'Class'],
                'TitanCrest'                => ['Titan Crest', true, 'Class'],
                'HunterCrest'               => ['Hunter Crest', true, 'Class'],
                'WarlockCrest'              => ['Warlock Crest', true, 'Class'],
                'CrucibleCrest'             => ['Crucible Crest', true, 'Class'],
                'LoneGuardian'              => ['The Lone Guardian', true, 'Character'],
                'ABray1'                    => ['Ana Bray 1', true, 'Character'],
                'ABray2'                    => ['Ana Bray 2', true, 'Character'],

                'ABray3'                    => ['Ana Bray 3', true, 'Character'],
                'ABray4'                    => ['Ana Bray 4', true, 'Character'],
                'ABray5'                    => ['Ana Bray 5', true, 'Character'],
                'ABray6'                    => ['Ana Bray 6', true, 'Character'],
                'IceThrall'                 => ['Ice Thrall', true, 'Enemy'],
                'Traveler'                  => ['Sunset Traveler', true, 'Traveler'],
                'Arcadia'                   => ['Arcadia Jumpship', true, 'Ship'],
                'HBNorse'                   => ['Happy Birthday Norsefenrir', false, 'Admin'],
            ],
            'SS11' => [
                'MinimationTitan'           => ['Destiny Minimation Titan', true, 'Misc'],
                'MinimationWarlock'         => ['Destiny Minimation Warlock', true, 'Misc'],
                'BlessingOfMomentum'        => ['Blessing of Momentum', true, 'Emblem'],
                'CrestOfVelocity'           => ['Crest of Velocity', true, 'Emblem'],
                'MartiusMomentum'           => ['Martius Momentum', true, 'Emblem'],
                'MercyOfMotion'             => ['Mercy of Motion', true, 'Emblem'],
                'SignOfMomentum'            => ['Sign of Momentum', true, 'Emblem'],
                'WinnersCircle'             => ['Winners Circle', true, 'Emblem'],

                'HouseOfJudgementEmbl'      => ['House of Judgement', true, 'Emblem'],
                'Wolfhunter'                => ['Wolfhunter', true, 'Emblem'],
                'EZS'                       => ['Zhalo Supercell', true, 'Weapon'],
                'AsherMir'                  => ['Asher Mir', true, 'Character'],
                'SRLQuestIcon'              => ['SRL Quest Icon', true, 'Activity'],
                'Mercury'                   => ['Mercury', true, 'Location'],
                'Venus'                     => ['Venus', true, 'Location'],
                'Earth'                     => ['Earth', true, 'Location'],
                
                'Luna'                      => ['Luna', true, 'Location'],
                'Mars'                      => ['Mars', true, 'Location'],
                'Jupiter'                   => ['Jupiter', true, 'Location'],
                'Saturn'                    => ['Saturn', true, 'Location'],
                'Phobos'                    => ['Phobos', true, 'Location'],
                'Cosmodrome'                => ['Cosmodrome', true, 'Location'],
                'OceanStorms'               => ['Ocean of Storms', true, 'Location'],
                'Ishtar'                    => ['Ishtar Sink', true, 'Location'],
                
                'Meridian'                  => ['Meridian Bay', true, 'Location'],
                'Plaguelands'               => ['Plaguelands', true, 'Location'],
                'FelwinterPeak'             => ['Felwinter Peak', true, 'Location'],
                'VostokObs'                 => ['Vostok Observatory', true, 'Location'],
                'Asylum'                    => ['Asylum', true, 'Location'],
                'Bannerfall'                => ['Bannerfall', true, 'Location'],
                'Bastion'                   => ['Bastion', true, 'Location'],
                'BlackShield'               => ['Black Shield', true, 'Location'],
                
                'BlindWatch'                => ['Blind Watch', true, 'Location'],
                'CathedralDusk'             => ['Cathedral of Dusk', true, 'Location'],
                'Crossroads'                => ['Crossroads', true, 'Location'],
                'ExodusBlue'                => ['Exodus Blue', true, 'Location'],
                'Delphi'                    => ['Firebase Delphi', true, 'Location'],
                'FirstLight'                => ['First Light', true, 'Location'],
                'FloatingGardens'           => ['Floating Gardens', true, 'Location'],
                'Frontier'                  => ['Frontier', true, 'Location'],
                
                'Icarus'                    => ['Icarus', true, 'Location'],
                'LastExit'                  => ['Last Exit', true, 'Location'],
                'Memento'                   => ['Memento', true, 'Location'],
                'Pantheon'                  => ['Pantheon', true, 'Location'],
                'RustedLands'               => ['Rusted Lands', true, 'Location'],
                'Sector618'                 => ['Sector 618', true, 'Location'],
                'ShoresOfTime'              => ['Shores of Time', true, 'Location'],
                'Skyline'                   => ['Skyine', true, 'Location'],
                
                'Skyshock'                  => ['Skyshock', true, 'Location'],
                'Anomaly'                   => ['The Anomaly', true, 'Location'],
                'BurningShrine'             => ['The Burning Shrine', true, 'Location'],
                'Cauldron'                  => ['The Cauldron', true, 'Location'],
                'Drifter'                   => ['The Drifter', true, 'Location'],
                'Dungeons'                  => ['The Dungeons', true, 'Location'],
                'Timekeeper'                => ['The Timekeeper', true, 'Location'],
                'ThieveDen'                 => ['Thieves\' Den', true, 'Location'],
                
                'TwilightGap'               => ['Twilight Gap', true, 'Location'],
                'Vertigo'                   => ['Vertigo', true, 'Location'],
                'WidowCourt'                => ['Widow\'s Court', true, 'Location'],
                'Qodron'                    => ['Qodron, Gatelord', true, 'Enemy'],
                'Theosyion'                 => ['Theosyion, Restorative Mind', true, 'Enemy'],
                'Darkblade'                 => ['Alak-Hul, the Darkblade', true, 'Enemy'],
                'ThreeWizards'              => ['Alzok Dal, Gornuk Dal, Zyrok Dal', true, 'Enemy'],
                'Balwur'                    => ['Balwur', true, 'Enemy'],
            ],
            'SS12' => [
                'OryxEcho'                  => ['Echo of Oryx', true, 'Enemy'],
                'Golgoroth'                 => ['Golgoroth', true, 'Enemy'],
                'Gulrot'                    => ['Gulrot, Unclean', true, 'Enemy'],
                'IrAnuk'                    => ['Ir Anuk, the Unraveler', true, 'Enemy'],
                'IrHalak'                   => ['Ir Halak, the Weaver', true, 'Enemy'],
                'Krughor'                   => ['Krughor', true, 'Enemy'],
                'Lokaar'                    => ['Lokaar', true, 'Enemy'],
                'Oryx1'                     => ['Oryx, the Taken King', true, 'Enemy'],
                
                'Oryx2'                     => ['Oryx Rebuked', true, 'Enemy'],
                'Oryx3'                     => ['Oryx Defeated', true, 'Enemy'],
                'Thalnok'                   => ['Thaknok, Fanatic of Crota', true, 'Enemy'],
                'Warpriest'                 => ['The Warpriest', true, 'Enemy'],
                'Urrox'                     => ['Uroxx, Flame Prince', true, 'Enemy'],
                'Vorlog'                    => ['Vorlog', true, 'Enemy'],
                'WretchedKn'                => ['Wretched Knight', true, 'Enemy'],
                'Aruun'                     => ['Val Aru\'un', true, 'Enemy'],
                
                'Mauual'                    => ['Valus Mau\'ual', true, 'Enemy'],
                'Tluurn'                    => ['Valus Tlu\'urn', true, 'Enemy'],
                'Trauug'                    => ['Valus Trau\'ug', true, 'Enemy'],
                'Kaliks'                    => ['Kaliks Reborn', true, 'Enemy'],
                'Kovik'                     => ['Kovik, Splicer Priest', true, 'Enemy'],
                'Paskin'                    => ['Paskin, King Baron', true, 'Enemy'],
                'Pilot'                     => ['Pilot Servitor', true, 'Enemy'],
                'SABER2'                    => ['S.A.B.E.R.-2', true, 'Enemy'],
                
                'SepiksPer'                 => ['Sepiks Perfected', true, 'Enemy'],
                'Skolas'                    => ['Skolas, Kell of Kells', true, 'Enemy'],
                'Taniks'                    => ['Taniks, the Scared', true, 'Enemy'],
                'Vekis'                     => ['Vekis, King Baron', true, 'Enemy'],
                'Vosik'                     => ['Vosik, the Archpriest', true, 'Enemy'],
                'Yavek'                     => ['Yavek, Wolf Baron', true, 'Enemy'],
                'Baxx'                      => ['Baxx, the Gravekeeper', true, 'Enemy'],
                'Horuusk'                   => ['Bracus Horu\'usk', true, 'Enemy'],
                
                'KEKSIS'                    => ['KEKSIS, THE BETRAYED', true, 'Enemy'],
                'Kagoor'                    => ['Kagoor', true, 'Enemy'],
                'Malok'                     => ['Malok, Pride of Oryx', true, 'Enemy'],
                'MengoorCraadug'            => ['Mengoor and Cra\'adug', true, 'Enemy'],
                'Noruusk'                   => ['Noru\'usk, Servant of Oryx', true, 'Enemy'],
                'Taaun'                     => ['Primus Ta\'aun', true, 'Enemy'],
                'Seditious'                 => ['Seditious Mind', true, 'Enemy'],
                'SYLOK'                     => ['SYLOK, THE DEFILED', true, 'Enemy'],
                
                'HezenC'                    => ['Hezen Corrective', true, 'Enemy'],
                'HezenP'                    => ['Hezen Protective', true, 'Enemy'],
                'Virgo'                     => ['Virgo Prohibition', true, 'Enemy'],
                'Divisive'                  => ['Sol Divisive', true, 'Enemy'],
                'Precursors'                => ['Precursors', true, 'Enemy'],
                'Descendants'               => ['Descendants', true, 'Enemy'],
                'HiddenSwarm'               => ['The Hidden Swarm', true, 'Enemy'],
                'CrotaSpawn'                => ['Spawn of Crota', true, 'Enemy'],
                
                'OryxBlood'                 => ['Blood of Oryx', true, 'Enemy'],
                'SandEaters'                => ['Sand Eaters', true, 'Enemy'],
                'DustGiants'                => ['Dust Giants', true, 'Enemy'],
                'SiegeDancers'              => ['Siege Dancers', true, 'Enemy'],
                'BlindLegion'               => ['Blind Legion', true, 'Enemy'],
                'Skyburners'                => ['Skyburners', true, 'Enemy'],
                'Devils'                    => ['House of Devils', true, 'Enemy'],
                'Exiles'                    => ['House of Exiles', true, 'Enemy'],
                
                'Winter'                    => ['House of Winter', true, 'Enemy'],
                'Kings'                     => ['House of Kings', true, 'Enemy'],
                'Wolves'                    => ['House of Wolves', true, 'Enemy'],
                'Splicers'                  => ['Devil Splicers', true, 'Enemy'],
                'SilentFang'                => ['The Silent Fang', true, 'Enemy'],
                'TheTaken'                  => ['The Taken', true, 'Enemy'],
                'Cyclops'                   => ['Cyclops', true, 'Enemy'],
                'Goblin'                    => ['Goblin', true, 'Enemy'],
            ],
            'SS13' => [
                'Hobgoblin'                 => ['Hobgoblin', true, 'Enemy'],
                'Harpy'                     => ['Harpy', true, 'Enemy'],
                'Minotaur'                  => ['Minotaur', true, 'Enemy'],
                'Hydra'                     => ['Hydra', true, 'Enemy'],
                'Dreg'                      => ['Dreg', true, 'Enemy'],
                'Vandal'                    => ['Vandal', true, 'Enemy'],
                'Captain'                   => ['Captain', true, 'Enemy'],
                'Shank'                     => ['Shank', true, 'Enemy'],
                
                'Servitor'                  => ['Servitor', true, 'Enemy'],
                'Thrall'                    => ['Thrall', true, 'Enemy'],
                'Acolyte'                   => ['Acolyte', true, 'Enemy'],
                'Knight'                    => ['Knight', true, 'Enemy'],
                'Wizard'                    => ['Wizard', true, 'Enemy'],
                'Ogre'                      => ['Ogre', true, 'Enemy'],
                'Shrieker'                  => ['Shrieker', true, 'Enemy'],
                'Legionary'                 => ['Legionary', true, 'Enemy'],
                
                'Phalanx'                   => ['Phalanx', true, 'Enemy'],
                'Psion'                     => ['Psion', true, 'Enemy'],
                'Centurion'                 => ['Centurion', true, 'Enemy'],
                'Colossus'                  => ['Colossus', true, 'Enemy'],
                'Overmind'                  => ['Overmind Minotaur', true, 'Enemy'],
                'TAcolyte'                  => ['Taken Acolyte', true, 'Enemy'],
                'TCaptain'                  => ['Taken Captain', true, 'Enemy'],
                'TCenturion'                => ['Taken Centurion', true, 'Enemy'],
                
                'TGobin'                    => ['Taken Goblin', true, 'Enemy'],
                'TKnight'                   => ['Taken Knight', true, 'Enemy'],
                'TMinotaur'                 => ['Taken Minotaur', true, 'Enemy'],
                'TPhalanx'                  => ['Taken Phalanx', true, 'Enemy'],
                'TPsion'                    => ['Taken Psion', true, 'Enemy'],
                'TThrall'                   => ['Taken Thrall', true, 'Enemy'],
                'TVandal'                   => ['Taken Vandal', true, 'Enemy'],
                'TWizard'                   => ['Taken Wizard', true, 'Enemy'],
                
                'THobgoblin'                => ['Taken Hobgoblin', true, 'Enemy'],
                'CFrames'                   => ['City Frames', true, 'Character'],
                'Efrideet'                  => ['Lady Efrideet', true, 'Character'],
                'Tyra'                      => ['Tyra Karn', true, 'Character'],
                'Shiro4'                    => ['Shiro-4, Vanguard Scout', true, 'Character'],
                'Micha'                     => ['Micha 99-40, Bountytracker', true, 'Character'],
                'Gabi'                      => ['Gabi 55-20, Postmaster', true, 'Character'],
                'Petra'                     => ['Petra Venj, Queen\'s Wrath', true, 'Character'],
                
                'Variks'                    => ['Variks, the Loyal', true, 'Character'],
                'Ives'                      => ['Master Ives', true, 'Character'],
                'Vance'                     => ['Brother Vance', true, 'Character'],
                'RoyalGuard'                => ['Royal Awoken Guard', true, 'Character'],
                'RFrames'                   => ['Reef Frames', true, 'Character'],
                'Coven'                     => ['The Coven', true, 'Character'],
                'TGrimoire'                 => ['Titan Grimoire', true, 'Class'],
                'HGrimoire'                 => ['Hunter Grimoire', true, 'Class'],
                
                'WGrimoire'                 => ['Warlock Grimoire', true, 'Class'],
                'Guardians'                 => ['Guardians', true, 'Class'],
                'Lighthouse'                => ['The Lighthouse', true, 'Location'],
                'CrimsonDays'               => ['Crimson Days Tower', true, 'Location'],
                'FestivalLost'              => ['Festival of the Lost Tower', true, 'Location'],
                'TheDawning'                => ['The Dawning Tower', true, 'Location'],
                'Aegis'                     => ['The Aegis', true, 'Misc'],
                'Sword'                     => ['Ascendant Sword', true, 'Misc'],
                
                'GGrimoire'                 => ['Ghost', true, 'Ghost'],
                'SRLlogo'                   => ['SRL Logo', true, 'Activity'],
                'Aksis'                     => ['Aksis, Archon Prime', true, 'Enemy'],
                'Regicide'                  => ['Regicide', true, 'Activity'],
                'AsteroidBelt'              => ['The Asteroid Belt', true, 'Location'],
                'Reef'                      => ['The Reef', true, 'Location'],
                'Vesta'                     => ['Vestian Outpost', true, 'Location'],
                'Prison'                    => ['Prison of Elders', true, 'Location'],
            ],
            'SS14' => [
                'SUROSY2WPN'                => ['SUROS Regime Y2', true, 'Weapon'],
                'KhvostovWPN'               => ['Khvostov 7G-0X', true, 'Weapon'],
                'OutbreakWPN'               => ['Outbreak Prime', true, 'Weapon'],
                'RedDeathY2WPN'             => ['Red Death Y2', true, 'Weapon'],
                'TimeToExplainWPN'          => ['No Time To Explain', true, 'Weapon'],
                'FirstCurseWPN'             => ['The First Curse', true, 'Weapon'],
                'TouchMaliceWPN'            => ['Touch of Malice', true, 'Weapon'],
                'D1JadeRabbitWPN'           => ['D1 Jade Rabbit', true, 'Weapon'],
                
                'BooleanWPN'                => ['Boolean Gemini', true, 'Weapon'],
                'TrespasserWPN'             => ['Trespasser', true, 'Weapon'],
                'DregPromiseWPN'            => ['Dreg\'s Promise', true, 'Weapon'],
                'D1TelestoWPN'              => ['D1 Telesto', true, 'Weapon'],
                'QueenbreakerWPN'           => ['Queenbreaker\'s Bow', true, 'Weapon'],
                'HereafterWPN'              => ['Hereafter', true, 'Weapon'],
                'ZenMeteorWPN'              => ['Zen Meteor', true, 'Weapon'],
                'NLBY2WPN'                  => ['No Land Beyond Y2', true, 'Weapon'],
                
                'SpindleWPN'                => ['Black Spindle', true, 'Weapon'],
                'ChaperoneWPN'              => ['The Chaperone', true, 'Weapon'],
                'IronGjallarWPN'            => ['Iron Gjallarhorn', true, 'Weapon'],
                'NovaWPN'                   => ['Nova Mortis', true, 'Weapon'],
                'AbbadonWPN'                => ['Abbadon', true, 'Weapon'],
                'NemesisWPN'                => ['Nemesis Star', true, 'Weapon'],
                'RazeLighterWPN'            => ['Raze-Lighter', true, 'Weapon'],
                'DarkDrinkerWPN'            => ['Dark-Drinker', true, 'Weapon'],
                
                'BoltCasterWPN'             => ['Bolt-Caster', true, 'Weapon'],
                'WolfHowlWPN'               => ['Young Wolf\'s Howl', true, 'Weapon'],
                'FabianWPN'                 => ['Fabian Strategy', true, 'Weapon'],
                'AceSpadesWPN'              => ['Ace of Spades', true, 'Weapon'],
                'TlalocWPN'                 => ['Tlaloc', true, 'Weapon'],
                'FatebringerWPN'            => ['Fatebringer', true, 'Weapon'],
                'EpilogueWPN'               => ['Atheon\'s Epilogue', true, 'Weapon'],
                'TimepieceWPN'              => ['Praedyth\'s Timepiece', true, 'Weapon'],
                
                'ConfluenceWPN'             => ['Vision of Confluence', true, 'Weapon'],
                'WordCrotaWPN'              => ['Word of Crota', true, 'Weapon'],
                'DefiantWPN'                => ['Abyss Defiant', true, 'Weapon'],
                'EdictWPN'                  => ['Oversoul Edict', true, 'Weapon'],
                'IrYutWPN'                  => ['Fang of Ir Y�t', true, 'Weapon'],
                'ZaouliWPN'                 => ['Zaouli\'s Bane', true, 'Weapon'],
                'DyrstanWPN'                => ['Anguish of Dyrstan', true, 'Weapon'],
                'MerainWPN'                 => ['Smite of Merain', true, 'Weapon'],
                
                'ChelchisWPN'               => ['Doom of Chelchis', true, 'Weapon'],
                'FeverRemedyWPN'            => ['FEVER AND REMEDY', true, 'Weapon'],
                'GenesisWPN'                => ['GENESIS CHAIN', true, 'Weapon'],
                'MedullaWPN'                => ['STEEL MEDULLA', true, 'Weapon'],
                'DogmaWPN'                  => ['CHAOS DOGMA', true, 'Weapon'],
                'StrangerWPN'               => ['The Stranger\'s Rifle', true, 'Weapon'],
                'MurmurWPN'                 => ['Murmur', true, 'Weapon'],
                'DynastyWPN'                => ['Vestian Dynasty', true, 'Weapon'],
                
                'NormalCoin'                => ['Perfectly Normal Coin', true, ''],
                'D1StrangeCoin'             => ['D1 Strange Coin', true, ''],
                'D2StrangeCoin'             => ['D2 Strange Coin', true, ''],
                'SpinfoilHat'               => ['Spinfoil Hat', true, ''],
                false,
                false,
                false,
                false,
                
                'D2Warlock'                 => ['D2 Warlock', true, 'Class'],
                'D2Titan'                   => ['D2 Titan', true, 'Class'],
                'D2Hunter'                  => ['D2 Hunter', true, 'Class'],
                'OsirisReflection'          => ['Reflection of Osiris', true, 'Character'],
                'CaydeDrink'                => ['Last Call', true, 'Character'],
                'UldrenHype'                => ['HYPE', true, 'Character'],
                'ZavalaFaceApp'             => ['Not-Creepy Zavala', true, 'Character'],
                'ZavalaEyes'                => ['Zavala Eyebeams', true, 'Character'],
            ],
            'SS15' => [
                'D2SUROSWPN'                => ['D2 SUROS Regime', true, 'Weapon'],
                'HuckleberryWPN'            => ['The Huckleberry', true, 'Weapon'],
                'PolarisWPN'                => ['Polaris Lance', true, 'Weapon'],
                'WorldlineWPN'              => ['Worldline Zero', true, 'Weapon'],
                'D2SleeperWPN'              => ['D2 Sleeper Simulant', true, 'Weapon'],
                'AlchemicalGhost'           => ['Alchemical Dawn Ghost', true, 'Ghost'],
                'WisdomGhost'               => ['Bursting Wisdom Ghost', true, 'Ghost'],
                'KnightsGhost'              => ['Knight\'s Peace Ghost', true, 'Ghost'],
                
                'IrisPulsatorGhost'         => ['Iris Pulsator Ghost', true, 'Ghost'],
                'SkylineGhost'              => ['Skyline Flipside Ghost', true, 'Ghost'],
                'HolobornShip'              => ['Holoborn\'s Splint Ship', true, 'Ship'],
                'LostLegendShip'            => ['Lost Legend Ship', true, 'Ship'],
                'TrespassShip'              => ['Shadow Trespass Ship', true, 'Ship'],
                'StarkBafflerShip'          => ['Stark Baffler Ship', true, 'Ship'],
                'ZavalaShip'                => ['Zavala\'s Authority Ship', true, 'Ship'],
                'VespulaserSparrow'         => ['Vespulaser Sparrow', true, 'Sparrow'],
                
                'OthersideSparrow'          => ['Otherside Sparrow', true, 'Sparrow'],
                'AnserisOverdriveSparrow'   => ['G-335 Anseris Overdrive Sparrow', true, 'Sparrow'],
                'EonDriveSparrow'           => ['Eon Drive Sparrow', true, 'Sparrow'],
                'AzazyelSparrow'            => ['Azure Azazyel Sparrow', true, 'Sparrow'],
                'ImpactVelocitySparrow'     => ['Impact Velocity Sparrow', true, 'Sparrow'],
                'TiltFuseSparrow'           => ['Tilt Fuse Sparrow', true, 'Sparrow'],
                'TrichromaticaGhost'        => ['Trichromatica Ghost', true, 'Ghost'],
                'WavefunctionShip'          => ['Universal Wavefunction Ship', true, 'Ship'],
                
                'ToastEmote'                => ['Luxurious Toast Emote', true, 'Emote'],
                'FireworksEmote'            => ['Fireworks Emote', true, 'Emote'],
                'PopcornEmote'              => ['Popcorn Emote', true, 'Emote'],
                'LordsSaluteEmote'          => ['Salute of the Lords Emote', true, 'Emote'],
                'CurtainCallEmote'          => ['Curtain Call Emote', true, 'Emote'],
                'HomeRunEmote'              => ['Home Run Emote', true, 'Emote'],
                'ColdheartCat'              => ['Coldheart Catalyst', true, 'Weapon'],
                'HardLightCat'              => ['Hard Light Catalyst', true, 'Weapon'],
                
                'GravitonCat'               => ['Graviton Lance Catalyst', true, 'Weapon'],
                'CrimsonCat'                => ['Crimson Catalyst', true, 'Weapon'],
                'PolarisCat'                => ['Polaris Lance Catalyst', true, 'Weapon'],
                'SunshotCat'                => ['Sunshot Catalyst', true, 'Weapon'],
                'VigilanceCat'              => ['Vigilance Wing Catalyst', true, 'Weapon'],
                'PrometheusCat'             => ['Prometheus Lens Catalyst', true, 'Weapon'],
                'MIDACat'                   => ['MIDA Multitool Catalyst', true, 'Weapon'],
                'JadeRabbitCat'             => ['Jade Rabbit Catalyst', true, 'Weapon'],
                
                'HuckleberryCat'            => ['The Huckleberry Catalyst', true, 'Weapon'],
                'SturmCat'                  => ['Sturm Catalyst', true, 'Weapon'],
                'SkyburnerCat'              => ['Skyburner\'s Oath Catalyst', true, 'Weapon'],
                'RiskrunnerCat'             => ['Riskrunner Catalyst', true, 'Weapon'],
                'RatKingCat'                => ['Rat King Catalyst', true, 'Weapon'],
                'SweetBusinessCat'          => ['Sweet Business Catalyst', true, 'Weapon'],
                'SUROSCat'                  => ['SUROS Regime Catalyst', true, 'Weapon'],
                'BorealisCat'               => ['Borealis Catalyst', true, 'Weapon'],
                
                'AcriusCat'                 => ['Legend of Acrius Catalyst', true, 'Weapon'],
                'DarciCat'                  => ['D.A.R.C.I. Catalyst', true, 'Weapon'],
                'ColonyCat'                 => ['The Colony Catalyst', true, 'Weapon'],
                'FightingLionCat'           => ['Fighting Lion Catalyst', true, 'Weapon'],
                'MercilessCat'              => ['Merciless Catalyst', true, 'Weapon'],
                'SleeperCat'                => ['Sleeper Simulant Catalyst', true, 'Weapon'],
                'ProspectorCat'             => ['The Prospector Catalyst', true, 'Weapon'],
                'WardcliffCat'              => ['Wardcliff Coil Catalyst', true, 'Weapon'],
                
                'TractorCat'                => ['Tractor Cannon Catalyst', true, 'Weapon'],
                'TelestoCat'                => ['Telesto Catalyst', true, 'Weapon'],
                'WorldlineCat'              => ['Worldline Zero Catalyst', true, 'Weapon'],
                'SpindleCat'                => ['Black Spindle Catalyst', true, 'Weapon'],
                'RedrixWPN'                 => ['Redrix\'s Claymore', true, 'Weapon'],
                'NeuromaWPN'                => ['Silicon Neuroma', true, 'Weapon'],
                'DFAWPN'                    => ['D.F.A.', true, 'Weapon'],
                'DutyBoundWPN'              => ['Duty Bound', true, 'Weapon'],
            ],
        ],
    );

    public static function is_class_available($css_class) {
        $parts = explode(' ', $css_class);

        if (count($parts) !== 3) {
            return false;
        }

        if ( empty(self::CONFIG['sheets'][$parts[0]][$parts[2]]) ) {
            return false;
        }

        if ( self::CONFIG['sheets'][$parts[0]][$parts[2]][1] !== true ) {
            return false;
        }

        return true;
    }

    public static function is_valid_class($css_class) {
        $parts = explode(' ', $css_class);

        if (count($parts) < 2 || 3 < count($parts)) {
            return false;
        }

        // check sheet (part 0)
        if (empty(self::CONFIG['sheets'][$parts[0]])) {
            return false;
        }

        // check posxy (part 1)
        $pos = explode('-', $parts[1]);

        if (count($pos) != 2) {
            return false;
        }

        if (!valid_int($pos[0]) || !valid_int($pos[1])) {
            return false;
        }

        $pos[0] = intval($pos[0]);
        $pos[1] = intval($pos[1]);

        if ($pos[0] < 0 || $pos[1] < 0) {
            return false;
        }

        if ($pos[0] >= self::CONFIG['sheet']['columns']) {
            return false;
        }

        if ($pos[1] >= self::CONFIG['sheet']['rows']) {
            return false;
        }

        // check original flair class (part 2)
        if (count($parts) == 3) {
            if ( empty(self::CONFIG['sheets'][$parts[0]][$parts[2]]) ) {
                return false;
            }
        }

        return true;
    }

    public static function create_css($for_display=false, $use_scale=false) {
        $conf = self::CONFIG;

        $scale = $use_scale ? $conf['sheet']['scale'] : 1;
        $sheet_width = $conf['sheet']['width'] * $scale;
        $sheet_height = $conf['sheet']['height'] * $scale;
        $cell_width = $conf['cell']['width'] * $scale;
        $cell_height = $conf['cell']['height'] * $scale;

        $css = ".flair{display:block;width:{$cell_width}px;height:{$cell_height}px;}";

        for ($col = 0; $col < $conf['sheet']['columns']; $col++) {
            for ($row = 0; $row < $conf['sheet']['rows']; $row++) {
                $xpos = $col * $cell_width;
                $ypos = $row * $cell_height;

                if ($xpos == 0 && $ypos == 0) {
                    $css .= '.flair-'.$col.'-'.$row.'{background-position: 0px 0px}';
                    continue;
                }

                $css .= '.flair-'.$col.'-'.$row.'{background:-'.$xpos.'px -'.$ypos.'px}';
            }
        }

        $css .= "\n";
        $css .= ".flair{background-size:".$sheet_width."px ".$sheet_height."px !important}";
        $css .= "\n";

        $images = FlairManager::get_stylesheet_images();

        foreach (array_keys($conf['sheets']) as $sheet_name) {
            if (!isset($images[$sheet_name])) {
                continue;
            }
            $css .= '.flair-'.$sheet_name.'{background-image:';
            if ($for_display) {
                $css .= 'url('.$images[$sheet_name]['url'].')';
            } else {
                $css .= $images[$sheet_name]['link'];
            }
            $css .= "!important}\n";
        }

        return $css;
    }
}
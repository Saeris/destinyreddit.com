<?php
namespace app\lib;

class DiscordMod {
    private static $client = null;

    public static function get_client() {
        if (isset(self::$client)) {
            return self::$client;
        }

        autoload_oauth2_client([]);

        self::$client = new \OAuth2Client\Client(
            OAUTH_DISCORDMOD_CLIENT_ID,
            OAUTH_DISCORDMOD_CLIENT_SECRET,
            \OAuth2Client\Client::AUTH_TYPE_FORM,
            CACERT_FILE
        );
        self::$client->setCurlOption(CURLOPT_USERAGENT, OAUTH_DISCORDMOD_USER_AGENT);
        self::$client->setAccessToken(OAUTH_DISCORDMOD_TOKEN);
        self::$client->setAccessTokenType(\OAuth2Client\Client::ACCESS_TOKEN_BOT);

        return self::$client;
    }

    public static function get_user($user_id) {
        $user = self::get_client()->fetch(
            'https://discordapp.com/api/guilds/'.DISCORD_GUILD_ID.'/members/'.$user_id)['result'];
        $user['is_admin'] = (count(array_intersect($user['roles'] ?? [], DISCORD_ACP_WHITELIST)) >= 1);
        return $user;
    }

    public static function post_message($channel_id, $content) {
        return self::get_client()->fetch('https://discordapp.com/api/channels/'.$channel_id.'/messages', array(
            'content' => $content,
        ), \OAuth2Client\Client::HTTP_METHOD_POST);
    }


}
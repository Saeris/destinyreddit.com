<?php
namespace app\lib;

require_once APP_ROOT . 'lib/minify/src/Minify.php';
require_once APP_ROOT . 'lib/minify/src/CSS.php';
require_once APP_ROOT . 'lib/minify/src/JS.php';
require_once APP_ROOT . 'lib/minify/src/Exception.php';
require_once APP_ROOT . 'lib/minify/src/Exceptions/BasicException.php';
require_once APP_ROOT . 'lib/minify/src/Exceptions/FileImportException.php';
require_once APP_ROOT . 'lib/minify/src/Exceptions/IOException.php';
require_once APP_ROOT . 'lib/path-converter/src/ConverterInterface.php';
require_once APP_ROOT . 'lib/path-converter/src/Converter.php';

use \MatthiasMullie\Minify;

class Minifier {

    public static function minify_css($css) {
        $minifier = new Minify\CSS();
        $minifier->add($css);
        return $minifier->minify();
    }
}
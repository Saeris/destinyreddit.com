<?php
namespace app\lib\admin;

use app\lib\RedditMod;
use app\lib\Minifier;

class StylesheetCTL {

    public static function save($pkg_name, $changes, $order) {
        $delete_queue = [];
        $insert_queue = [];
        $enabled_queue = [];
        $styles_queue = [];

        $delete_success = null;
        $insert_success = null;
        $enabled_success = null;
        $styles_success = null;

        foreach ($changes as $mod_name => $properties) {
            $deleted    = from($properties, 'deleted');
            $enabled    = from($properties, 'enabled');
            $styles     = from($properties, 'styles');
            $new_module = from($properties, 'new_module');

            if ($deleted === true) {
                $delete_queue[] = [$pkg_name, $mod_name];
                continue;
            }

            if ($new_module === true) {
                $insert_queue[] = [
                    'pkg_name' => $pkg_name,
                    'mod_name' => $mod_name,
                    'enabled'  => $enabled ?? true,
                    'styles'   => $styles ?? '',

                    'created_utc'  => time(),
                    'modified_utc' => time(),
                    'build_order'  => 1,
                ];
                continue;
            }

            if (isset($enabled)) {
                $enabled_queue[] = [$pkg_name, $mod_name, $enabled];
            }

            if (isset($styles)) {
                $styles_queue[] = [$pkg_name, $mod_name, $styles];
            }
        }

        if (!empty($delete_queue)) {
            db('application')->query(
                'DELETE FROM "dtg_css_modules"
                WHERE (pkg_name, mod_name) in (%ll?_items) AND build_order!=0',
                [
                    'items' => $delete_queue
                ]);
            $delete_success = count($delete_queue) == db('application')->affectedRows();
        }

        if (!empty($insert_queue)) {
            db('application')->insertIgnore('dtg_css_modules', $insert_queue);
            $insert_success = count($insert_queue) == db('application')->affectedRows();
        }

        if (!empty($enabled_queue)) {
            db('application')->query(
                'UPDATE dtg_css_modules AS t SET
                    enabled = c.enabled
                FROM (VALUES %ll?_queue) as c(pkg_name, mod_name, enabled)
                WHERE t.pkg_name = c.pkg_name AND t.mod_name = c.mod_name',
                ['queue' => $enabled_queue]);
            $enabled_success = count($enabled_queue) == db('application')->affectedRows();

        }

        if (!empty($styles_queue)) {
            db('application')->query(
                'UPDATE dtg_css_modules AS t SET
                    styles = c.styles
                FROM (VALUES %ll?_queue) as c(pkg_name, mod_name, styles)
                WHERE t.pkg_name = c.pkg_name AND t.mod_name = c.mod_name',
                ['queue' => $styles_queue]);
            $styles_success = count($styles_queue) == db('application')->affectedRows();
        }

        $order_queue = [];
        $order_i = 1;
        foreach ($order as $mod_name) {
            $order_queue[] = [$pkg_name, $mod_name, $order_i++];
        }

        if (!empty($order_queue)) {
            db('application')->query(
                'UPDATE dtg_css_modules AS t SET
                    build_order = c.build_order
                FROM (VALUES %ll?_queue) as c(pkg_name, mod_name, build_order)
                WHERE t.pkg_name = c.pkg_name AND t.mod_name = c.mod_name',
                ['queue' => $order_queue]);
            $order_success = count($order_queue) == db('application')->affectedRows();
        }

        return [
            'delete'  => $delete_success,
            'insert'  => $insert_success,
            'enabled' => $enabled_success,
            'styles'  => $styles_success,
        ];
    }

    public static function deploy($package, $only_css = false) {
        $base_styles = self::get_base_CSS($package);
        $user_styles = '';

        $modules = self::get_modules($package);

        if ($base_styles === false) {
            return false;
        }

        foreach ($modules[0] as $pkg_name => $mod_files) {
            if ($pkg_name != $package) {
                continue;
            }
            foreach ($mod_files as $mod_name => $mod_data) {
                if ($mod_data['mod_type'] != 'user_module') {
                    continue;
                }
                if ($mod_data['enabled'] !== true) {
                    continue;
                }
                $user_styles .= $mod_data['styles'];
            }
        }

        $user_styles = Minifier::minify_css($user_styles);
        $styles = $base_styles . "\n" . $user_styles;

        if ($only_css) {
            return $styles;
        }

        $url = "https://oauth.reddit.com/r/{$package}/api/subreddit_stylesheet/";
        $params = [
            'api_type'  => 'json',
            'op'        => 'save',
            'reason'    => 'Deployed via StylesheetCTL by /u/' . get_username(),
            'stylesheet_contents' => $styles
        ];

        return RedditMod::get_client()->fetch($url, $params, 'POST');
    }

    public static function get_base_CSS($package) {
        return file_get_contents(CSS_REPO_DIR.'dist/'.$package.'.css');
    }

    public static function get_modules($package = null) {
        $mod_dir    = CSS_REPO_DIR.'mod/' . ($package ?? '');
        $mod_dirl   = strlen($mod_dir);
        $mod_files  = rglob($mod_dir, 'css');
        $mod_tree   = [];
        $mod_list   = [];
        $mod_names  = [];

        natsort($mod_files);

        foreach ($mod_files as $f) {
            list($pkg_name, $mod_name) = explode(DIRECTORY_SEPARATOR, substr(strval($f), $mod_dirl, -4));

            $mod_list[] = [$pkg_name, $mod_name];

            if (!isset($mod_tree[$pkg_name])) {
                $mod_tree[$pkg_name] = [];
                $mod_names[$pkg_name] = [];
            }

            $mod_tree[$pkg_name][$mod_name] = [
                'pkg_name'  => $pkg_name,
                'mod_name'  => $mod_name,
                'enabled'   => true,
                'styles'    => file_get_contents(strval($f)),
                'mod_type'  => 'core_module',
            ];
            $mod_names[$pkg_name][] = $mod_name;
        }

        $enabled_list = db('application')->query(
            'SELECT "pkg_name", "mod_name", "enabled" FROM "dtg_css_modules"
            WHERE (pkg_name, mod_name) in (%ll?_items) AND build_order=0',
            [ 'items' => $mod_list ]);
        foreach ($enabled_list as $row) {
            if (!isset($mod_tree[$row['pkg_name']]) || !isset($mod_tree[$row['pkg_name']][$row['mod_name']]))
                continue;
            $mod_tree[$row['pkg_name']][$row['mod_name']]['enabled'] = $row['enabled'];
        }

        foreach ($mod_names as $tmp1 => $tmp2) {
            // push the number of core modules into the first spot
            array_unshift($mod_names[$tmp1], count($tmp2));
        }

        $pkg_part = '';
        if (isset($package)) {
            $pkg_part = 'AND pkg_name=%s';
        }

        $user_modules = db('application')->query(
            "SELECT * FROM dtg_css_modules
            WHERE build_order!=0 {$pkg_part}
            ORDER BY build_order ASC
            NULLS LAST", $package);
        foreach ($user_modules as $module) {
            $pkg_name = $module['pkg_name'];
            $mod_name = $module['mod_name'];

            $mod_tree[$pkg_name][$mod_name] = [
                'pkg_name'  => $pkg_name,
                'mod_name'  => $mod_name,
                'enabled'   => $module['enabled'],
                'styles'    => $module['styles'],
                'mod_type'  => 'user_module',
            ];

            $mod_names[$pkg_name][] = $mod_name;
        }

        return [$mod_tree, $mod_names];
    }

}
<?php
namespace app\lib;

class DiscordClient {
    public static function logged_in() {
        return isset($_SESSION['d_logged_in']) && $_SESSION['d_logged_in'] === true;
    }

    public static function get_username() {
        return from($_SESSION, 'd_username');
    }

    public static function get_id() {
        return from($_SESSION, 'd_id');
    }

    public static function get_discriminator() {
        return from($_SESSION, 'd_discriminator');
    }

    public static function switchSession($username, $id, $discriminator, $cont=null) {
        session_regenerate_id();

        $_SESSION['d_username'] = strval($username);
        $_SESSION['d_id'] = strval($id);
        $_SESSION['d_discriminator'] = strval($discriminator);
        $_SESSION['d_logged_in'] = true;
        $_SESSION['d_is_admin'] = null;

        redirect(isset($cont) ? $cont : '/clan-recruitment');
    }

    public static function failure($error = null) {
        if ($error == 'access_denied') {
            redirect_cont();
        }

        view('net.loginfailed')
            ->meta('Login failed', 'page--login_failed')
            ->stylesheets('inlogin')
            ->using([
                'error'         => $error,
                'relogin_link'  => '/discord-login',
            ])->dispatch();
        die();
    }

    public static function authorize() {
        autoload_oauth2_client();

        $client = new \OAuth2Client\Client(
            OAUTH_DISCORD_CLIENT_ID,
            OAUTH_DISCORD_CLIENT_SECRET,
            \OAuth2Client\Client::AUTH_TYPE_FORM,
            CACERT_FILE
        );
        $client->setCurlOption(CURLOPT_USERAGENT, OAUTH_DISCORD_USER_AGENT);

        if (isset($_REQUEST['error']))
            self::failure($_REQUEST['error']);
        if (isset($_REQUEST['cont']))
            $_SESSION['d_cont_after_login'] = $_REQUEST['cont'];

        if (!isset($_SESSION['d_access_token'])) {
            if (!isset($_GET["code"])) {
                redirect_ext( $client->getAuthenticationUrl(
                    OAUTH_DISCORD_AUTHORIZE_URL,
                    OAUTH_DISCORD_REDIRECT_URL,
                    array(
                        "scope"     => "identify",
                        "state"     => safe_token(),
                        "duration"  => "permanent"
                    )
                ) );
            } else if (isset($_GET["code"])) {
                $response = $client->retrieveAccessToken(
                    OAUTH_DISCORD_ACCESS_TOKEN_URL,
                    "authorization_code",
                    array(
                        "code" => $_GET["code"],
                        "redirect_uri" => OAUTH_DISCORD_REDIRECT_URL
                    ),
                    array(
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    )
                );

                $accessTokenResult = $response["result"];

                if (!isset($accessTokenResult["access_token"])) {
                    self::failure("unable to retrieve access token");
                }

                $client->setAccessToken($accessTokenResult["access_token"]);
                $client->setAccessTokenType(\OAuth2Client\Client::ACCESS_TOKEN_BEARER);

                $_SESSION['d_access_token']  = $accessTokenResult["access_token"];
                $_SESSION['d_refresh_token'] = $accessTokenResult["refresh_token"];

                // Now let's try to get the Reddit user's username
                $userdata = $client->fetch("https://discordapp.com/api/users/@me");

                if (!isset($userdata['result']['id']) || !isset($userdata['result']['username'])) {
                    self::failure("we were unable to retrieve your username/id from the Discord API");
                }

                self::switchSession(
                    $userdata['result']['username'],
                    $userdata['result']['id'],
                    $userdata['result']['discriminator'],
                    from($_SESSION, 'd_cont_after_login')
                );
            } else {
                echo "Authorization failed.";
            }

        } else {
            if (isset($_SESSION['d_cont_after_login'])) {
                $cont = $_SESSION['d_cont_after_login'];
                unset($_SESSION['d_cont_after_login']);
                redirect($cont);
            } else {
                redirect('/clan-recruitment');
            }
            die();
        }
    }

    public static function get_client() {
        static $_client = null;

        if (isset($_client)) {
            return $_client;
        }

        if (!isset($_SESSION['d_logged_in']) || $_SESSION['d_logged_in'] !== true) {
            return null;
        }

        $response = self::refreshToken();
        return $_client = $response['client'];
    }

    public static function refreshToken() {
        if (!empty($_SESSION['d_refresh_token'])) {
            autoload_oauth2_client();

            $params = array("refresh_token" => $_SESSION['d_refresh_token']);

            $client = new \OAuth2Client\Client(
                OAUTH_DISCORD_CLIENT_ID,
                OAUTH_DISCORD_CLIENT_SECRET,
                \OAuth2Client\Client::AUTH_TYPE_FORM,
                CACERT_FILE
            );

            $client->setCurlOption(CURLOPT_USERAGENT, OAUTH_DISCORD_USER_AGENT);
            $response = $client->retrieveAccessToken(OAUTH_DISCORD_ACCESS_TOKEN_URL, 'refresh_token', $params);

            $client->setAccessToken($response["result"]["access_token"]);
            $client->setAccessTokenType(\OAuth2Client\Client::ACCESS_TOKEN_BEARER);

            $_SESSION['d_access_token'] = $response["result"]["access_token"];

            return ['response' => $response, 'client' => $client];
        } else {
            session_destroy();
            echo '<html><head></head><body>Internal Error: your session was lost for reasons unknown.&nbsp;' .
                '<a href="'.SITE_URL.'discord-login">Click here to log back in.</a></body></html>';
            die();
        }
    }
}
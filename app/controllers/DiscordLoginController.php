<?php
namespace app\controllers;

use app\lib\DiscordClient;

class DiscordLoginController extends \Controller {

    public function __construct() {
        parent::__construct(func_get_args());
    }

    protected function default_action(array $params) {
        return false;
    }

    public function login(array $params) {
        DiscordClient::authorize(); // also handles redirect
    }

    public function logout(array $params) {
        session_destroy();
        redirect_cont();
    }

}
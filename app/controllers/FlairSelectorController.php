<?php
namespace app\controllers;

use app\lib\flair\FlairConfig;
use app\lib\flair\FlairManager;
use app\lib\flair\FlairChangeResult;

class FlairSelectorController extends \Controller {

    public function __construct() {
        parent::__construct(func_get_args());
    }

    protected function performSetFlair() {
        $data = [
            'flair_class'   => from($_REQUEST, 'flair_class') ?: '',
            'flair_text'    => from($_REQUEST, 'flair_text') ?: '',
            'flair_user'    => get_username(),
            'subreddit'     => FlairManager::SUBREDDIT,
        ];

        if (!logged_in()) {
            $data['error'] = 'you must be logged in';
            return $data;
        }

        if (is_admin() && isset($_REQUEST['flair_user'])) {
            $data['flair_user'] = $_REQUEST['flair_user'];
        }

        $message = FlairManager::set_flair($data['flair_user'], $data['flair_class'], $data['flair_text']);

        if ($message !== FlairChangeResult::SUCCESS) {
            $data['error'] = $message;
            return $data;
        }

        return $data;
    }

    protected function submit(array $params) {
        $data = $this->performSetFlair();
        $data['flair_css'] = FlairConfig::create_css(true);

        return view('flair.flairsubmitted')
            ->meta('DTG Flair Selector', 'page--flair page--flairsubmitted')
            ->stylesheets('flair/flairselector')
            ->addCSP('img-src', FlairManager::get_stylesheet_image_urls())
            ->using($data);
    }

    protected function default_action(array $params) {
        $data = $this->json(true);

        return view('flair.flairselector')
            ->meta('DTG Flair Selector', 'page--flair page--flairselector')
            ->stylesheets('flair/flairselector')
            ->addCSP('img-src', FlairManager::get_stylesheet_image_urls())
            ->usinglib('lunr')
            ->using([
                'subreddit'     => FlairManager::SUBREDDIT,
                'flair_css'     => FlairConfig::create_css(true),
                'flair_data'    => $data,
            ])
            ->scripts('flair/flairselector')
            ->addInitOpt('FlairSelector', 'disabled', !logged_in());
    }

    protected function json($params) {
        $json = [];

        $col_max = FlairConfig::CONFIG['sheet']['columns']-1;

        foreach (FlairConfig::CONFIG['sheets'] as $sheet_name => $flair_templates) {
            if (empty($flair_templates))
                continue;

            $col = 0;
            $row = 0;

            foreach ($flair_templates as $name => $data) {
                $flair_class = $sheet_name . ' ' . ($col . '-' . $row) . ' ' . $name;
                $flair_class2 =
                    'flair-'.$sheet_name .
                    ' flair-' . ($col . '-' . $row) .
                    ' flair-' . $name;

                if (($col++) == $col_max) {
                    $col = 0;
                    $row++;
                }

                if (empty($data)) continue;

                $json[$name] = [
                    'name'          => $name,
                    'display_name'  => $data[0],
                    'flair_class'   => $flair_class,
                    'flair_class2'  => $flair_class2,
                    'available'     => $data[1],
                    'category'      => $data[2] ?: 'none',
                ];
            }
        }

        if ($params === true) {
            return $json;
        }

        dispatch_json($json);
    }

}
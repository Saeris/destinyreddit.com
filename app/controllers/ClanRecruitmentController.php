<?php
namespace app\controllers;

use app\lib\DiscordMod;
use app\lib\clans\RecruitForm;
use app\lib\clans\ManualQueue;
use app\lib\clans\Blacklist;

class ClanRecruitmentController extends \Controller {

    public function __construct() {
        parent::__construct(func_get_args());
    }

    protected function default_action(array $params) {
        return view('clans.recruitform')
            ->meta('DTG Discord Clan Recruitment Form', 'page--discord page--clans page--clans_recruitform')
            ->stylesheets('clans/recruitform')
            ->scripts('clans/recruitform')
            ->addInitOpt('RecruitForm', 'logged_in', discord_logged_in())
            ->addInitOpt('RecruitForm', 'submitted', false);
    }

    protected function submit(array $params) {
        $data = RecruitForm::cursory_validate($_POST);

        return view('clans.recruitform')
            ->meta('DTG Discord Clan Recruitment Form', 'page--discord page--clans page--clans_recruitform')
            ->stylesheets('clans/recruitform')
            ->scripts('clans/recruitform')
            ->using([
                'cursory_validate_failed'   => ($data === false),
                'submit_response'           => RecruitForm::process($data),
                'submitted'                 => ($data !== false),
            ])
            ->addInitOpt('RecruitForm', 'logged_in', discord_logged_in())
            ->addInitOpt('RecruitForm', 'submitted', $data !== false);
    }

    protected function admin($route, $params = []) {
        if (!discord_is_admin()) {
            return view('clans.admin')
                ->meta('Clan-Recruitment ACP', 'page--discord page--d_admin')
                ->stylesheets('clans/admin')
                ->using([
                    'acp_title' => discord_logged_in() ? 'Access Denied' : 'Login Required',
                    'page'      => '403',
                ] + $params);
        }

        $is_POST = $route['METHOD'] == 'POST' && isset($_POST['action']);

        switch ($page = ($route['page'] ?? 'home')) {
            case 'home':
                $page_title = 'ACP Home';
                break;
            case 'queue':
                if ($is_POST && isset($_POST['app_id'])) {
                    switch($_POST['action']) {
                        case 'deny':
                            RecruitForm::submit_to_denied($_POST['app_id'], 'Denied by moderator.', get_discord_userping());
                            break;
                        case 'accept':
                            RecruitForm::submit_to_channel($_POST['app_id'], 'Accepted by moderator.', get_discord_userping());
                            break;
                    }
                    redirect(current_url());
                }

                $page_title = 'Manual Queue';
                $params['apps'] = ManualQueue::get_apps();
                break;
            case 'blacklist':
                if ($is_POST) {
                    switch($_POST['action']) {
                        case 'add':
                            Blacklist::add_to_blacklist($_POST['fieldtype'], $_POST['fieldval']);
                            break;
                        case 'remove':
                            Blacklist::remove_from_blacklist($_POST['id']);
                            break;
                    }
                    redirect(current_url());
                }

                $page_title = 'Blacklist';
                $params['blacklist'] = Blacklist::get_blacklisted();
                break;
            case 'history':
                $page_title = 'History';
                $params += ManualQueue::get_history( intval($_GET['page'] ?? 1) );
                break;
            default:
                $page = '404';
                $page_title = '404';
                break;
        }

        return view('clans.admin')
            ->meta('Clan-Recruitment ACP', 'page--discord page--d_admin')
            ->stylesheets('clans/admin')
            ->using([
                'acp_title' => $page_title,
                'page'      => $page,
            ] + $params);
    }

}
<?php
namespace app\controllers;

use app\lib\RedditMod;
use app\lib\flair\FlairManager;
use app\lib\flair\FlairConfig;

class AdminController extends \Controller {

    public function __construct() {
        parent::__construct(func_get_args());
        $this->prepend_middleware(function($params) {
            if (!logged_in() || !is_admin()) {
                return view('net.error.403')->title('Access Denied');
            }
        });
    }

    protected function default_action(array $params) {
        header('Content-Type: text/html');

        switch ($params['name']) {
            case 'test_oauth':
                $client = RedditMod::get_client();
                $result = $client->fetch("https://oauth.reddit.com/api/v1/me.json");
                var_dump($result);
                break;
            case 'test_set_flair':
                var_dump(FlairManager::set_flair('kwwxis', 'test-class', 'text '.time()));
                break;
            case 'get_stylesheet_images':
                var_dump(FlairManager::get_stylesheet_images());
                break;
            case 'get_stylesheet_image_urls':
                var_dump(FlairManager::get_stylesheet_image_urls());
                break;
            case 'force_stylesheet_images_cache_expiry':
                FlairManager::force_stylesheet_images_cache_expiry();
                echo "Done.";
                break;
            case 'create_css':
                echo FlairConfig::create_css( isset($_REQUEST['for_display']), isset($_REQUEST['use_scale']) );
                break;
            case 'test_500_error_page':
                invoke_500_error_page();
            case 'test_invoke_error':
                echo $variable_that_doesnt_exist;
                break;
            default:
                return false;
        }

        die;
    }

}
<?php
// BASIC
// --------------------------------------------------------------------------------
const SITE_NAME = 'destinyreddit';
const SITE_HOST = 'destinyreddit.com';
const SITE_URL = 'https://destinyreddit.com/';
const SITE_API = 'https://destinyreddit.com/';
const SITE_TITLE = 'DestinyReddit';
const SITE_STATIC = 'https://static.layer7.solutions/';
const SITE_IS_STAGING = false;

// ASSETS HTTP LOCATIONS
// --------------------------------------------------------------------------------
const SITE_ASSETS = 'https://destinyreddit.com/assets/';
const SITE_JS = 'https://destinyreddit.com/assets/js/';
const SITE_CSS = 'https://destinyreddit.com/assets/css/';
const SITE_IMAGES = 'https://destinyreddit.com/assets/images/';

// ASSETS FILE LOCATIONS
// --------------------------------------------------------------------------------
const ASSETS_ROOT = '/var/www/staging.destinyreddit.com/public/assets/';
const JS_ROOT = '/var/www/staging.destinyreddit.com/public/assets/js/';
const CSS_ROOT = '/var/www/staging.destinyreddit.com/public/assets/css/';
const IMAGES_ROOT = '/var/www/staging.destinyreddit.com/public/assets/images/';
const APP_ROOT = '/var/www/staging.destinyreddit.com/app/';
const ROOT_PATH = '/var/www/staging.destinyreddit.com/';
const BOOTSTRAP_ROOT = '/var/www/beta.layer7.solutions/bootstrap/';

// DEFAULT VIEWS
// --------------------------------------------------------------------------------
const VIEWS_ROOT = '/var/www/staging.destinyreddit.com/app/views/';
const VIEWS_500 = '/var/www/staging.destinyreddit.com/app/views/net/error/500.html';
const VIEWS_LAYOUT = 'net.layout';

// LAYER7
// --------------------------------------------------------------------------------
const LAYER7_SITE_NAME = 'www';
const LAYER7_SITE_ASSETS = 'https://layer7.solutions/assets/';
const LAYER7_SITE_JS = 'https://layer7.solutions/assets/js/';
const LAYER7_SITE_CSS = 'https://layer7.solutions/assets/css/';
const LAYER7_SITE_IMAGES = 'https://layer7.solutions/assets/images/';

// SECURITY
// --------------------------------------------------------------------------------
const CACERT_FILE = '/var/www/cacert.pem';

// REDDIT OAUTH
// --------------------------------------------------------------------------------
const OAUTH_AUTHORIZE_URL = 'https://ssl.reddit.com/api/v1/authorize';
const OAUTH_ACCESS_TOKEN_URL = 'https://www.reddit.com/api/v1/access_token';

const OAUTH_CLIENT_ID = '<REDACTED>';
const OAUTH_CLIENT_SECRET = '<REDACTED>';
const OAUTH_USER_AGENT = '<REDACTED>';
const OAUTH_REDIRECT_URL = '<REDACTED>';

const OAUTH_MODERATOR_USERNAME = '<REDACTED>';
const OAUTH_MODERATOR_PASSWORD = '<REDACTED>';
const OAUTH_MODERATOR_ID = '<REDACTED>';
const OAUTH_MODERATOR_SECRET = '<REDACTED>';
const OAUTH_MODERATOR_USER_AGENT = '<REDACTED>';
const OAUTH_MODERATOR_REDIRECT_URL = '<REDACTED>';

// DISCORD OAUTH
// --------------------------------------------------------------------------------
const OAUTH_DISCORD_AUTHORIZE_URL = 'https://discordapp.com/api/oauth2/authorize';
const OAUTH_DISCORD_ACCESS_TOKEN_URL = 'https://discordapp.com/api/oauth2/token';
const OAUTH_DISCORD_CLIENT_ID = '<REDACTED>';
const OAUTH_DISCORD_CLIENT_SECRET = '<REDACTED>';
const OAUTH_DISCORD_USER_AGENT = '<REDACTED>';
const OAUTH_DISCORD_REDIRECT_URL = '<REDACTED>';

const OAUTH_DISCORDMOD_CLIENT_ID = '<REDACTED>';
const OAUTH_DISCORDMOD_CLIENT_SECRET = '<REDACTED>';
const OAUTH_DISCORDMOD_TOKEN = '<REDACTED>';
const OAUTH_DISCORDMOD_USER_AGENT = '<REDACTED>';

// Production:
//   /r/DestinyTheGame Guild ID:   157728722999443456
//   #channel-recruitment:         369994284067454976
//   #mod-only                     255099898897104908
// Testing:
//   Test guild:                   305133671776649216
//   Test channel:                 305138344021590016
const DISCORD_GUILD_ID = '305133671776649216';
const DISCORD_CLAN_RECRUITMENT_CHANNEL_ID = '305138344021590016';
const DISCORD_MOD_ONLY_CHANNEL_ID = '305138344021590016';
const DISCORD_ACP_WHITELIST = [
    '305135063996170251', // @Moderators - DEV SERVER role id
    '302255737302679552', // @Moderators - PROD SERVER role id
    '385198309897273345', // "kwwxis test role"
];

// DATABASE
// --------------------------------------------------------------------------------
const PGSQL_SERVER = '<REDACTED>';
const PGSQL_USERNAME = '<REDACTED>';
const PGSQL_PASSWORD = '<REDACTED>';
const DB_DEFAULT_DRIVER = '<REDACTED>';

// MEMCACHED
// --------------------------------------------------------------------------------
const MEMCACHED_HOST = '<REDACTED>';
const MEMCACHED_PORT = '<REDACTED>';
const MEMCACHED_PREFIX = 'www_';

// MISC
// --------------------------------------------------------------------------------
const CSS_REPO_DIR = '<REDACTED>/dtg-css/';